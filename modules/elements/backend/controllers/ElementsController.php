<?php

namespace app\modules\elements\backend\controllers;
use Yii;
use app\modules\elements\backend\models\ElementsSearch;
use app\modules\elements\common\models\Elements;

use app\modules\core\backend\components\ARController;

class ElementsController extends ARController
{

    public function beforeAction($action)
    {
        $this->model_class = Banners::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['create'], $actions['update']);

        return $actions;
    }

    protected function getModelClass()
    {
        return Elements::className();
    }

    protected function getSearchModelClass()
    {          
        return ElementsSearch::className();
    }
    public function actionUpdate($id)
    {
        /** @var Elements $model */
        $model = Elements::findOne($id);
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } 

        
        return $this->render('update', [
            'model' => $model,
        ]);
    }


}
