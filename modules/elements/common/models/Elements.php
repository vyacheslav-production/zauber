<?php

namespace app\modules\elements\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "mp_banners".
 *
 * @property integer $mp_elements_id
 * @property string  $mp_elements_name
 * @property string  $mp_elements_content
 */
class Elements extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_elements';
    }
    public function rules()
    {
        return [
            [['mp_elements_name'], 'string', 'max' => 255],
            [['mp_elements_content'], 'string', 'max' => 65536],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_elements_id'       => 'ID',
            'mp_elements_name'     => 'Название',
            'mp_elements_content'  => 'Контент',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->mp_elements_id;
    }

    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new ElementsQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
