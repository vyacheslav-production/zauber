<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\core\backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends LTEAdminAsset
{
    public $css = ['/web/backend/assets/ec116040/admin-lte/plugins/iCheck/square/yellow.css'];
    public $js = ['/web/backend/assets/ec116040/admin-lte/plugins/iCheck/icheck.min.js'];
    public $depends = [
        'app\modules\core\backend\assets\AppAsset'
    ];
}
