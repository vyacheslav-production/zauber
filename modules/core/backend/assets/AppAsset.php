<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\core\backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
   public $baseUrl = '@web';

//    public $basePath = '/web/backend';
//    public $baseUrl = '/web/backend';
//	public $sourcePath = '@webroot/assets';

    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/backend.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\modules\core\backend\assets\LTEAdminAsset'
    ];
}
