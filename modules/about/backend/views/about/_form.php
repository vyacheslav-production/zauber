<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use app\modules\core\common\models\Regions;

use app\modules\about\common\models\About;

/* @var $this yii\web\View */
/* @var $model app\modules\about\common\models\About */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'about_title')->textInput(['maxlength' => 50]) ?>
    <?= $form->field($model, 'about_name')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'about_city')->dropDownList((
                Regions::getDropDown()
    )) ?>

<?= $form->field($model, 'about_prk_1')->dropDownList(ArrayHelper::map(About::listRecord1(['about_city'=>$model->about_city]), 'about_id', function($item) {
    return $item->about_name;
}), ['size' => 1, 'prompt'=>'Не выбрана','onChange'=> 'javascript:fillPrk2(this.value,"'.$model->about_city.'")'] ) ?>
<?= $form->field($model, 'about_prk_2')->dropDownList(ArrayHelper::map(About::listRecord(['about_city'=>$model->about_city]), 'about_id', function($item) {
    return $item->about_name;
}), ['size' => 1, 'prompt'=>'Не выбрана']) ?>
    <?= $form->field($model, 'about_context')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
