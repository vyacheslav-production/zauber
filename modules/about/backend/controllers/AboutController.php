<?php

namespace app\modules\about\backend\controllers;

use app\modules\core\backend\components\ARController;
use Yii;
use app\modules\about\common\models\About;
use app\modules\about\backend\models\AboutSearch;
use app\modules\core\backend\components\Controller;

/**
 * AboutController implements the CRUD actions for About model.
 */
class AboutController extends ARController
{

    protected function getModelClass()
    {   
        return About::className();
    }

    protected function getSearchModelClass()
    {
        return AboutSearch::className();
    }


}
