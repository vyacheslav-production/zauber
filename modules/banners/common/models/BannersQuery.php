<?php

namespace app\modules\banners\common\models;

use app\modules\core\common\models\Status;
use yii\db\ActiveQuery;

class BannersQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}