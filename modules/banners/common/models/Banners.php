<?php

namespace app\modules\banners\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "mp_banners".
 *
 * @property integer $mp_banners_id
 * @property string  $mp_banners_img
 * @property string  $mp_banners_link
 * @property string  $mp_banners_header
 * @property string  $mp_banners_text
 */
class Banners extends ActiveRecord
{

    static $image_width     = 1200;
    static $image_height    = 600;
    static $limit = 1;

    public $mp_slider_img   = NULL;
    public $mp_services_img   = NULL;
    public $mp_unions_img   = NULL;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_banners';
    }
    public function rules()
    {
        return [
            [['mp_banners_img'], 'string', 'max' => 255],
            [['mp_banners_link'], 'string', 'max' => 255],
            [['mp_banners_header'], 'string', 'max' => 255],
            [['mp_banners_text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_banners_id'        => 'ID',
            'mp_banners_img'       => 'Изображение',
            'mp_banners_link'      => 'Ссылка',
            'mp_banners_header'    => 'Заголовок',
            'mp_banners_text'      => 'Текст',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->mp_banners_id;
    }

    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new BannersQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
