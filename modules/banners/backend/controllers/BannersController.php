<?php

namespace app\modules\banners\backend\controllers;
use Yii;
use app\modules\banners\backend\models\BannersSearch;
use app\modules\banners\common\models\Banners;
use app\modules\core\backend\components\UploadForm;

use app\modules\core\backend\components\ARController;

class BannersController extends ARController
{

    public function beforeAction($action)
    {
        $this->model_class = Banners::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['create'], $actions['update']);

        return $actions;
    }

    protected function getModelClass()
    {
        return Banners::className();
    }

    protected function getSearchModelClass()
    {          
        return BannersSearch::className();
    }
    public function actionUpdate($id)
    {
        /** @var Banners $model */
        $model = Banners::findOne($id);
        $upload_form = new UploadForm(Banners::class);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } 

        
        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }


}
