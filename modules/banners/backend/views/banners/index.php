<?php

use yii\grid\GridView;

/* @var $this app\modules\core\backend\components\View */
/* @var $searchModel app\modules\banners\backend\models\BannersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннеры быстрого доступа';
$this->subTitle = 'Список';
$this->params['breadcrumbs'][] = $this->title;

// $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить', 'url' => ['create'], 'options' => ['class' => 'btn btn-success']];
// $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-trash-o"></i> Корзина', 'url' => ['recycle-bin'], 'options' => ['class' => 'btn btn-warning']];

?>
<div class="user-index">
    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'columns' => [
		[                     
	            'label' => 'Id',
	            'attribute' =>'mp_banners_id',
        	],
		[                     
	            'label' => 'Изображение',
	            'attribute' => 'mp_banners_img',
        	],
		[                     
	            'label' => 'Ссылка',
	            'attribute' => 'mp_banners_link',
        	],
		[                     
	            'label' => 'Заголовок',
	            'attribute' => 'mp_banners_header',
        	],
		[                     
	            'label' => 'Текст',
	            'attribute' => 'mp_banners_text',
        	],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}',]
        ],
    ]); ?>

</div>
