<?php

use yii\helpers\Html;
Use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\partner\common\models\Partner */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */

?>

<br><br>
<?= $form->field($model, 'mp_banners_link')->textInput() ?>
<?= $form->field($model, 'mp_banners_header')->textInput() ?>
<?= $form->field($model, 'mp_banners_text')->textInput() ?>
<div class="clearfix">

    <div class="row">
        <div class="col-md-6 contain_img">
            <?= Html::activeHiddenInput($model, 'mp_banners_img', ['class' => 'mp_banners_img'])?>
            <?= $form->field($upload_form, 'mp_banners_img')->fileInput(['class' => 'image_uploads', 'data-field' => 'mp_banners_img', 'data-url' => Url::to(['ajax-upload'])]) ?>
            <div class="for_img">
                <?php if($model->mp_banners_img): ?>
                    <?= $this->render('@core/views/image/_image', ['model' => $model, 'property' => 'mp_banners_img']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>