<?php

namespace app\modules\news\common\models;

use yii\db\ActiveQuery;

class NewsQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}