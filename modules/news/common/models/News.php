<?php

namespace app\modules\news\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "news".
 *
 * @property integer $news_id
 * @property string  $news_title
 * @property string  $news_section
 * @property string  $news_img
 * @property string  $news_content
 * @property integer $news_main_page_show
 */
class News extends ActiveRecord
{

    static $image_width     = 1200;
    static $image_height    = 600;
    static $limit = 1;

    public static function tableName()
    {
        return 'news';
    }
    public function rules()
    {
        return [
            [['news_title'], 'string', 'max' => 255],
            [['news_section'], 'string', 'max' => 255],
            [['news_img'], 'string', 'max' => 255],
            [['news_content'], 'string', 'max' => 65536],
            [['news_main_page_show'], 'integer', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_id'               => 'ID',
            'news_title'            => 'Название',
            'news_section'          => 'Раздел',
            'news_img'              => 'Изображение',
            'news_content'          => 'Контент',
            'news_main_page_show'   => 'Показыать на главной',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->news_id;
    }

    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }
    public function afterFind()
    {
//	$this->news_content = substr($this->news_content, 0, 50);
    }
    public static function getRussianDate($date)
    {
        $english_month = date('j F Y',strtotime($date));
	$trans = array("January" => "января",
               "February" => "февраля",
               "March" => "марта",
               "April" => "апреля",
               "May" => "мая",
               "June" => "июня",
               "July" => "июля",
               "August" => "августа",
               "September" => "сентября",
               "October" => "октября",
               "November" => "ноября",
               "December" => "декабря");

  	$result  = strtr( $english_month, $trans);
  	return $result;


    }
}   
