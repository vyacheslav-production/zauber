<?php

return [
    '/<city:\w+>/news/<page:\w+>' => '/news/news/index/',
    '/<city:\w+>/news/<year:\d+>/<month:\d+>/<day:\d+>/<id:\d+>' => '/news/news/onenews/',
];