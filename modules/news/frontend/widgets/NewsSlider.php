<?php

namespace app\modules\news\frontend\widgets;

use app\modules\news\common\models\News;
use yii\base\Widget;
use yii\helpers\Url;

class NewsSlider extends Widget
{
    public $template = 'newsslider';

    public function run()
    {

            $slider = News::find()->orderBy('news_date DESC')->limit(3)->all();

            return $this->render($this->template, ['slider' => $slider]);
    }
}