<?php

namespace app\modules\news\frontend\widgets;

use app\modules\news\common\models\News;
use yii\base\Widget;
use yii\helpers\Url;

class NewsPaginator extends Widget
{
    public $template = 'newspaginator';

    public function run()
    {

	$exploded = explode("/",Url::to());
	$city = $exploded[1];



            $slider_first_page = News::find()->orderBy('news_date DESC')->limit(10)->all();
            $slider_second_page = News::find()->orderBy('news_date DESC')->limit(10)->offset(10)->all();
            $slider_third_page = News::find()->orderBy('news_date DESC')->limit(10)->offset(20)->all();
            return $this->render($this->template, ['city' => $city, 'slider_first_page' => $slider_first_page, 'slider_second_page' => $slider_second_page, 'slider_third_page' => $slider_third_page]);
    }
}