<?php

namespace app\modules\news\frontend\widgets;

use app\modules\news\common\models\News;
use yii\base\Widget;
use yii\helpers\Url;

class NewsRightList extends Widget
{
    public $template = 'newsrightlist';

    public function run()
    {

	$exploded = explode("/",Url::to());
	$city = $exploded[1];

            $rightlist = News::find()->orderBy('news_date DESC')->limit(5)->all();

            return $this->render($this->template, ['rightlist' => $rightlist, 'city' => $city]);
    }
}