<?php
use app\modules\news\common\models\News;
?>

<h4>Свежие новости</h4>
	<ul class="news-list2">
	<?php foreach ($rightlist as $item) { ?>
		<li>
			<span class="date"><time datetime="01-01"><?=News::getRussianDate($item['news_date'])?></time></span>
			<p><a href="/<?=$city?>/news/<?=date('Y',strtotime($item['news_date']))?>/<?=date('m',strtotime($item['news_date']))?>/<?=date('d',strtotime($item['news_date']))?>/<?=$item['id']?>/"><?=$item['news_title']?></a></p>
		</li>
	<?php } ?>
	</ul>
