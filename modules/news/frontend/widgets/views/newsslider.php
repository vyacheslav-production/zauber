<?php
use app\modules\news\common\models\News;
?>
<ul class="slide-list">
<?php foreach ($slider as $item) { ?>

	<li style="background-image:url(<?=$item['news_img']?>);">
		<a href="#">
			<span class="news-content">
				<span class="date"><time datetime="01-01"><?=News::getRussianDate($item['news_date'])?></time></span>
				<span class="title"><?=$item['news_title']?></span>
			</span>
		</a>
	</li>
<?php } ?>
</ul>
