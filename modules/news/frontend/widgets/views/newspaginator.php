<?php
use app\modules\news\common\models\News;
?>
<ul class="slide-list">
	<li>
		<ul class="news-list2">
			<?php foreach ($slider_first_page as $item) { ?>
				<li>
					<span class="date"><time datetime="01-01"><?=News::getRussianDate($item['news_date'])?></time></span>
					<p><a href="/<?=$city?>/news/<?=date('Y',strtotime($item['news_date']))?>/<?=date('m',strtotime($item['news_date']))?>/<?=date('d',strtotime($item['news_date']))?>/<?=$item['id']?>/"><?=$item['news_title']?></a></p>
				</li>
			<?php } ?>
		</ul>
	</li>
	<li>
		<ul class="news-list2">
			<?php foreach ($slider_second_page as $item) { ?>
				<li>
					<span class="date"><time datetime="01-01"><?=News::getRussianDate($item['news_date'])?></time></span>
					<p><a href="/<?=$city?>/news/<?=date('Y',strtotime($item['news_date']))?>/<?=date('m',strtotime($item['news_date']))?>/<?=date('d',strtotime($item['news_date']))?>/<?=$item['id']?>/"><?=$item['news_title']?></a></p>
				</li>
			<?php } ?>
		</ul>
	</li>
	<li>
		<ul class="news-list2">
			<?php foreach ($slider_third_page as $item) { ?>
				<li>
					<span class="date"><time datetime="01-01"><?=News::getRussianDate($item['news_date'])?></time></span>
					<p><a href="/<?=$city?>/news/<?=date('Y',strtotime($item['news_date']))?>/<?=date('m',strtotime($item['news_date']))?>/<?=date('d',strtotime($item['news_date']))?>/<?=$item['id']?>/"><?=$item['news_title']?></a></p>
				</li>
			<?php } ?>
		</ul>
	</li>
</ul>
