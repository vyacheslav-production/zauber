<?php

namespace app\modules\news\frontend\widgets;

use app\modules\newsmenu\common\models\Newsmenu;
use yii\base\Widget;
use yii\helpers\Url;

class NewsUpperMenu extends Widget
{
    public $template = 'newsuppermenu';

    public function run()
    {

	$exploded = explode("/",Url::to());
	$city = $exploded[1];

        $menu = Newsmenu::find()->orderBy('news_menu_order DESC')->all();

        return $this->render($this->template, ['menu' => $menu, 'city' => $city]);
    }
}