<?php

namespace app\modules\news\frontend\controllers;

use app\modules\news\common\models\News;
use app\modules\volumes\common\models\Volumes;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

use yii\filters\VerbFilter;

class NewsController extends \yii\web\Controller
{
  public $layout='news.php';

    public function actionIndex($city='',$page='')
    {
     
       // all news in this section for right side widget
            $news_all = News::find()
            ->andWhere(['news_section' => $page ])
            ->all();

       // three news for left side slider
            $news_slider = News::find()
            ->orderBy('news_date DESC')
            ->limit(3)
            ->all();

            return $this->render('index',[
            'news_all' => $news_all,
            'news_slider' => $news_slider,
        ]);

    }
    public function actionOnenews($year=NULL, $month=NULL, $day=NULL, $id=NULL)
    {
     
       // certain news item
            $news_one = News::find()
            ->andWhere(['news_date' => $year.'-'.$month.'-'.$day])
            ->andWhere(['news_id' => $id])
            ->all();

            return $this->render('one',[
            'news_one' => $news_one,

        ]);


    }
}