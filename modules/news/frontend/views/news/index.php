<?php
use app\modules\core\frontend\widgets\MainPageSlider;
use app\modules\core\frontend\widgets\MainPageBanners;
use app\modules\core\frontend\widgets\MainPageUnions;
use app\modules\core\frontend\widgets\MainPageFooter;
use app\modules\core\frontend\widgets\MainPageShownMenu;
use app\modules\core\frontend\widgets\MainPageHiddenMenu;
use app\modules\core\frontend\widgets\MainPageServices;
use app\modules\core\frontend\widgets\MainPageNews;
use app\modules\core\frontend\widgets\MainPageCitySelect;
use app\modules\core\frontend\widgets\Search;
use app\modules\news\frontend\widgets\NewsSlider;
use app\modules\news\frontend\widgets\NewsPaginator;
?>
		<div id="wrapper">
			<header id="header">
				<div class="container">
					<div class="contact-panel">
						<strong class="logo"><a href="#">Заубер Банк</a></strong>
			                        <?= MainPageCitySelect::widget(); ?>
						<div class="contacts">
							<a href="tel:+78126470606" class="tel">+7 (812) <strong>647-06-06</strong></a>
							<div class="link-holder">
								<a href="#" class="link">Контактная информация</a>
							</div>
						</div>
						<div class="select-holder pull-right style2 unchanged">
							<select>
								<option>Для физических лиц</option>
								<option>Для юридических лиц</option>
							</select>
						</div>
						<a href="#" class="burger"><span>&nbsp;</span></a>
					</div>
				</div>
				<nav id="nav">
					<div class="container">
						<div class="nav-inner">
							<div class="change-list-holder">
								<ul class="change-list">
									<li class="changetype pli" clientType="private"><a href="#">Частным лицам</a></li>
									<li class="changetype bli" clientType="business"><a href="#">Для бизнеса</a></li>
								</ul>
							</div>
							<div class="menu-holder">
								<a href="#" class="close">X</a>
						                        <?= MainPageShownMenu::widget(); ?>
							</div>
							<div class="drop-holder">
								<a href="#" class="title">Другие услуги</a>
							</div>
				                        <?= Search::widget(); ?>
						</div>
					</div>
					<div class="drop-manu">
						<div class="container hiddenitems">
				                        <?= MainPageHiddenMenu::widget(); ?>
						</div>
					</div>
				</nav>
			</header>
			<div id="main">
				<div class="top-menu-holder">
					<div class="container">
						<ul class="top-menu">
							<li class="active"><a href="#">Новости банка</a></li>
							<li><a href="#">Пресса о нас</a></li>
							<li><a href="#">Новости партнеров</a></li>
						</ul>
					</div>
				</div>
				<div class="news-section2">
					<div class="heading" style="background-image:url(/images/bg2.jpg);">
						<div class="container">
							<h1>Новости банка</h1>
						</div>
					</div>
					<div class="container">
						<div class="content">
							<div class="news-item-slider">
							       <?= NewsSlider::widget(); ?>
							</div>
							<div class="news-slider">
							       <?= NewsPaginator::widget(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
                        <?= MainPageFooter::widget(); ?>
		</div>
	</body>
</html>