<?php
use app\modules\core\frontend\widgets\MainPageSlider;
use app\modules\core\frontend\widgets\MainPageBanners;
use app\modules\core\frontend\widgets\MainPageUnions;
use app\modules\core\frontend\widgets\MainPageFooter;
use app\modules\core\frontend\widgets\MainPageShownMenu;
use app\modules\core\frontend\widgets\MainPageHiddenMenu;
use app\modules\core\frontend\widgets\MainPageServices;
use app\modules\core\frontend\widgets\MainPageNews;
use app\modules\core\frontend\widgets\MainPageCitySelect;
use app\modules\core\frontend\widgets\Search;
use app\modules\news\frontend\widgets\NewsSlider;
use app\modules\news\frontend\widgets\NewsPaginator;
use app\modules\news\frontend\widgets\NewsUpperMenu;
use app\modules\news\frontend\widgets\NewsRightList;
use app\modules\news\common\models\News;
?>
		<div id="wrapper">
			<header id="header">
				<div class="container">
					<div class="contact-panel">
						<strong class="logo"><a href="#">Заубер Банк</a></strong>
			                        <?= MainPageCitySelect::widget(); ?>
						<div class="contacts">
							<a href="tel:+78126470606" class="tel">+7 (812) <strong>647-06-06</strong></a>
							<div class="link-holder">
								<a href="#" class="link">Контактная информация</a>
							</div>
						</div>
						<div class="select-holder pull-right style2 unchanged">
							<select>
								<option>Для физических лиц</option>
								<option>Для юридических лиц</option>
							</select>
						</div>
						<a href="#" class="burger"><span>&nbsp;</span></a>
					</div>
				</div>
				<nav id="nav">
					<div class="container">
						<div class="nav-inner">
							<div class="change-list-holder">
								<ul class="change-list">
									<li class="changetype pli" clientType="private"><a href="#">Частным лицам</a></li>
									<li class="changetype bli" clientType="business"><a href="#">Для бизнеса</a></li>
								</ul>
							</div>
							<div class="menu-holder">
								<a href="#" class="close">X</a>
						                        <?= MainPageShownMenu::widget(); ?>
							</div>
							<div class="drop-holder">
								<a href="#" class="title">Другие услуги</a>
							</div>
				                        <?= Search::widget(); ?>
						</div>
					</div>
					<div class="drop-manu">
						<div class="container hiddenitems">
				                        <?= MainPageHiddenMenu::widget(); ?>
						</div>
					</div>
				</nav>
			</header>
			<div id="main">
				<div class="top-menu-holder">
					<div class="container">
					       <?= NewsUpperMenu::widget() ?>
					</div>
				</div>
				<div class="news-section2">
					<div class="heading" style="background-image:url(images/bg2.jpg);">
						<div class="container">
							<h1>Новости банка</h1>
						</div>
					</div>
					<div class="container">
						<div class="long-news-item">
							<div class="visual">
							        <?php foreach ($news_one as $item) {?>
								<div class="img-holder"><img src="<?=$item['news_img'] ?>" alt="image description"></div>
								<div class="visual-content">
									<span class="date"><time datetime="01-01"><?= News::getRussianDate($item['news_date'])?></time></span>
									<span class="title"><?= $item['news_title']?></span>
								</div>
							</div>
							<div class="long-news-content">
								<div class="content">
									<?= $item['news_content']?>
								</div>
								<?php } ?>
								<div class="more-news-holder">
								       <?=NewsRightList::widget()?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
                        <?= MainPageFooter::widget(); ?>
		</div>
	</body>
</html>