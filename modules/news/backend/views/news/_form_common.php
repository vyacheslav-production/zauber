<?php

use yii\helpers\Html;
Use yii\helpers\Url;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;

use app\modules\newsmenu\common\models\NewsMenu;

/* @var $this yii\web\View */
/* @var $model app\modules\news\common\models\News */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */

?>

<br><br>
<?= $form->field($model, 'news_title')->textInput() ?>
<?= $form->field($model, 'news_section')->dropDownList(ArrayHelper::map(NewsMenu::listRecord(), 'news_menu_section', function($item) {
    return $item->news_menu_name;
}),['size' => 1] ) ?>

    <?= $form->field($model, 'news_content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced'
    ]) ?>
<div class="clearfix">

    <div class="row">
        <div class="col-md-6 contain_img">
            <?= Html::activeHiddenInput($model, 'news_img', ['class' => 'news_img'])?>
            <?= $form->field($upload_form, 'news_img')->fileInput(['class' => 'image_uploads', 'data-field' => 'news_img', 'data-url' => Url::to(['ajax-upload'])]) ?>
            <div class="for_img">
                <?php if($model->news_img): ?>
                    <?= $this->render('@core/views/image/_image', ['model' => $model, 'property' => 'news_img']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?= $form->field($model, 'news_main_page_show')->checkbox(['checked' => $model->news_main_page_show]) ?>