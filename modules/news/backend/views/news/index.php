<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\news\backend\models\News */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Новости';
$this->subTitle                = 'Список';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить',
                           'url'     => ['create'],
                           'options' => ['class' => 'btn btn-success']
];
$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-trash-o"></i> Корзина',
                           'url'     => ['recycle-bin'],
                           'options' => ['class' => 'btn btn-warning']
];
?>
<div class="program-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
		[                     
	            'label' => 'Id',
	            'attribute' =>'news_id',
        	],
		[                     
	            'label' => 'Название',
	            'attribute' => 'news_title',
        	],
		[                     
	            'label' => 'Раздел',
	            'value' => function($data) {
                		return ( $this->getPartName($data->news_section));
		            },

        	],
		[                     
	            'label' => 'Изображение',
	            'attribute' => 'news_img',
        	],
		[                     
	            'label' => 'Содержание',
	            'attribute' => 'news_content',
		    'value'=>array($this,'showFewLines')
        	],
		[                     
	            'label' => 'Показывать на главной',
	            'value' => function($data) {
                		return ( $this->getShowByWords($data->news_main_page_show));
		            },

        	],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>

</div>
