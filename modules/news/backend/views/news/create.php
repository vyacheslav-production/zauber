<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\news\common\models\News */

$this->title = 'Новости';
$this->subTitle = 'Добавить';

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->subTitle;
?>
<div class="program-create">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>