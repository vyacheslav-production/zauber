<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\news\common\models\News */

$this->title = 'Новости';
$this->subTitle = 'Редактирование: ' . $model->news_title;

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-bars"></i> Список', 'url' => ['index'], 'options' => ['class' => 'btn btn-success']];

?>
<div class="program-update">
    <?= $this->render('_form', ['model' => $model,'upload_form' => $upload_form,]) ?>

</div>
