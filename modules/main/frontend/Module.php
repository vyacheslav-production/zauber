<?php

namespace app\modules\main\frontend;

use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'app\modules\main\frontend\controllers';

    public function bootstrap($app)
    {
            $app->getUrlManager()->addRules(
                require(__DIR__ . '/config/routes.php'),
                false);

        $app->params['yii.migrations'][] = '@app/modules/main/migrations';
    }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
