<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_083133_reviews extends Migration
{
    private $table_name = '{{%reviews}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id'            => Schema::TYPE_PK,
            'active'        => Schema::TYPE_INTEGER,
            'sort'        => Schema::TYPE_INTEGER,
            'status'        => Schema::TYPE_INTEGER,
        ],$tableOptions);

        $this->createTable('{{%reviews_langs}}', [
            'reviews_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'lang_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'title'   => Schema::TYPE_STRING . '(255)',
            'text'    => Schema::TYPE_TEXT,
            'PRIMARY KEY (reviews_id, lang_id)',
            'FOREIGN KEY (reviews_id) REFERENCES reviews(id)',
            'FOREIGN KEY (lang_id) REFERENCES langs(id)',
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%reviews_langs}}');
        $this->dropTable($this->table_name);
    }
}
