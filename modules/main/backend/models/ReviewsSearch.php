<?php

namespace app\modules\main\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\main\common\models\Reviews;
use app\modules\main\common\models\ReviewsLangs;

/**
 * ReviewsSearch represents the model behind the search form about `app\modules\main\common\models\Reviews`.
 */
class ReviewsSearch extends Reviews
{
    public $title;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'active', 'sort', 'status'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reviews::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        
        $query->innerJoinWith(['translation']);

        $dataProvider->sort->attributes['title'] = [
            'asc'  => [ReviewsLangs::tableName() . '.title' => SORT_ASC],
            'desc' => [ReviewsLangs::tableName() . '.title' => SORT_DESC],
        ];
        
        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'sort' => $this->sort,
            'status' => $this->status,
        ]);

        
        $query->andFilterWhere(['like', ReviewsLangs::tableName() . '.title', $this->title]);

        
        return $dataProvider;
    }
}
