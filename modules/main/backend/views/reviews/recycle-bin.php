<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\main\backend\models\ReviewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Главная страницы';
$this->subTitle = 'Корзина';

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->subTitle;

$this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-bars"></i> Список', 'url' => ['index'], 'options' => ['class' => 'btn btn-success']];


echo Html::beginTag('div', ['class' => 'reviews-recycle-bin']);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'active',
            'sort',
            'status',
            [
                  'attribute' => 'title',
                  'value'     => 'translation.title',
            ],
            [
            'class'    => 'app\modules\core\backend\components\ActionColumn',
            'template' => '{restore} {erase}',
            ],
        ],
    ]);

echo Html::endTag('div');
