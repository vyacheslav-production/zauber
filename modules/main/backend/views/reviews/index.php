<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\main\backend\models\ReviewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Главная страницы';
$this->subTitle = 'Список';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить',
                           'url'     => ['create'],
                           'options' => ['class' => 'btn btn-success']
];
$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-trash-o"></i> Корзина',
                           'url'     => ['recycle-bin'],
                           'options' => ['class' => 'btn btn-warning']
];


echo Html::beginTag('div', ['class' => 'reviews-index']);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'active',
            'sort',
            'status',
            [
                  'attribute' => 'title',
                  'value'     => 'translation.title',
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]);

echo Html::endTag('div');
