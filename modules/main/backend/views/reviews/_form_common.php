<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\main\common\models\Reviews */
/* @var $form yii\widgets\ActiveForm */
?>

<br>

<?= $form->field($model, 'active')->checkbox(); ?>
<?= $form->field($model, 'sort')->textInput(); ?>

