<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\modules\main\common\models\ReviewsLangs */
/* @var $form yii\widgets\ActiveForm */

echo Html::beginTag('p');

echo $form->field($model, "[{$model->lang_id}]title");

echo $form->field($model, "[{$model->lang_id}]text")->widget(CKEditor::className(), [
    'preset' => 'full',
    'clientOptions' => [
        'allowedContent' => true
    ]
]) ;

echo Html::endTag('p');