<?php

return [
    'reviews/<action:[\w\-]+>' => '/main/reviews/<action>',
    'main/<action:[\w\-]+>' => '/main/main/<action>',
];