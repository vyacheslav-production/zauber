<?php

namespace app\modules\main\backend\controllers;

use Yii;
use app\modules\main\common\models\Reviews;
use app\modules\main\backend\models\ReviewsSearch;
use app\modules\core\backend\components\ARController;

/**
 * ReviewsController implements the CRUD actions for Reviews model.
 */
class ReviewsController extends ARController
{
    protected function getModelClass()
    {
        return Reviews::className();
    }

    protected function getSearchModelClass()
    {
        return ReviewsSearch::className();
    }
}
