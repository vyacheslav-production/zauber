<?php

namespace app\modules\main\common\models;

use Yii;
use app\modules\core\common\behaviors\SoftDeleteBehavior;
use app\modules\core\common\behaviors\TranslationBehavior;
use yii\behaviors\TimestampBehavior;
use app\modules\core\common\models\Lang;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property integer $id
 * @property integer $active
 * @property integer $sort
 * @property integer $status
 *
 * @property ReviewsLangs[] $reviewsLangs
 * @property Lang[] $langs
 */
class Reviews extends \app\modules\core\common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews}}';
    }

    public function behaviors()
    {
        return [
            'softDelete'          => [
                'class' => SoftDeleteBehavior::className()
            ],
            'translationBehavior' => [
                'class'              => TranslationBehavior::className(),
                'langModel'          => ReviewsLangs::className(),
                'langModelAttribute' => 'reviews_id'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'active'], 'default', 'value' => 1],
            [['sort'], 'default', 'value' => 500],
            [['active', 'sort', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
            'sort' => 'Сортировка',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewsLangs()
    {
        return $this->hasMany(ReviewsLangs::className(), ['reviews_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(Lang::className(), ['id' => 'lang_id'])->viaTable('{{%reviews_langs}}', ['reviews_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(ReviewsLangs::className(),
            ['reviews_id' => 'id'])->where([ReviewsLangs::tableName() . '.lang_id' => Lang::getCurrent()->id]);
    }

    public static function find()
    {
        return new ReviewsQuery(get_called_class());
    }
}
