<?php

namespace app\modules\main\common\models;

use Yii;
use app\modules\core\common\models\Lang;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%reviews_langs}}".
 *
 * @property integer $reviews_id
 * @property integer $lang_id
 * @property string $title
 * @property string $text
 *
 * @property Reviews $reviews
 * @property Lang $lang
 */
class ReviewsLangs extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews_langs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'lang_id'], 'required'],
            [['reviews_id', 'lang_id'], 'integer'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reviews_id' => 'Reviews ID',
            'lang_id' => 'Lang ID',
            'title' => 'Имя',
            'text' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasOne(Reviews::className(), ['id' => 'reviews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }
}
