<?php

namespace app\modules\main\common\models;

use Yii;
use app\modules\core\common\models\Status;
use yii\db\ActiveQuery;

class ReviewsQuery extends ActiveQuery
{

    public function init()
    {
        parent::init();
        $this->where(['!=', Reviews::tableName() . '.status', Status::ERASED]);
    }

    public function active($state = true)
    {
        $this->andWhere([Reviews::tableName() . '.status' => $state ? Status::ACTIVE : Status::DELETED]);
        return $this;
    }

}
