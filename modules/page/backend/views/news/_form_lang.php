<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<p>

    <?= $form->field($model, "[{$model->lang_id}]title")->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, "[{$model->lang_id}]announcement")->widget(CKEditor::className(), [
        'preset' => 'full',
        'clientOptions' => [
            'allowedContent' => true
        ]
    ]) ?>

    <?= $form->field($model, "[{$model->lang_id}]text")->widget(CKEditor::className(), [
        'preset' => 'full',
        'clientOptions' => [
            'allowedContent' => true
        ]
    ]) ?>

</p>