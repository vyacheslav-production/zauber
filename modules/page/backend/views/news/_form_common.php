<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $model app\modules\page\common\models\News
 * @var $upload_form app\modules\core\backend\components\UploadForm
 */
?>

<br><br>
<div class="clearfix">


    <div class="row">
        <?= $form->field($model, 'date', ['options' => ['class' => 'form-group col-md-4']])->widget(DatePicker::className(), [
            'name' => 'dp_1',
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>
    </div>

    <div class="row">
        <div class="col-md-6 contain_img">
            <?= Html::activeHiddenInput($model, 'image_primary', ['class' => 'image_primary'])?>
            <?= $form->field($upload_form, 'image_primary')->fileInput(['class' => 'image_uploads', 'data-field' => 'image_primary', 'data-url' => Url::to(['ajax-upload'])]) ?>
            <div class="for_img">
                <?php if($model->image_primary): ?>
                    <?= $this->render('@core/views/image/_image', ['model' => $model, 'property' => 'image_primary']) ?>
                <?php endif; ?>
            </div>

        </div>

        <div class="col-md-6 contain_img">
            <?= Html::activeHiddenInput($model, 'image_preview', ['class' => 'image_preview'])?>
            <?= $form->field($upload_form, 'image_preview')->fileInput(['class' => 'image_uploads', 'data-field' => 'image_preview', 'data-url' => Url::to(['ajax-upload'])]) ?>
            <div class="for_img">
                <?php if($model->image_preview): ?>
                    <?= $this->render('@core/views/image/_image', ['model' => $model, 'property' => 'image_preview']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>