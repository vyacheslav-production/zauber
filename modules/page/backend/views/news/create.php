<?php

use yii\helpers\Html;


/**
 * @var $this yii\web\View
 * @var $upload_form app\modules\core\backend\components\UploadForm
 * @var $model app\modules\page\common\models\News
 */

$this->title = 'Новости';
$this->subTitle = 'Добавить';

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->subTitle;
?>
<div class="page-create">

    <?= $this->render('_form', [
        'model' => $model,
        'upload_form' => $upload_form
    ]) ?>

</div>
