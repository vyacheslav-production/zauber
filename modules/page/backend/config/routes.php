<?php

return [
    'news/<action:[\w\-]+>' => '/page/news/<action>',
    'page/<action:[\w\-]+>' => '/page/page/<action>',
];