<?php

namespace app\modules\page\backend\controllers;

use app\modules\page\backend\models\NewsSearch;
use app\modules\page\common\models\News;
use app\modules\core\backend\components\UploadForm;
use app\modules\core\backend\components\ARController;
use Yii;

/**
 * PageController implements the CRUD actions for Page model.
 */
class NewsController extends ARController
{
    public function beforeAction($action)
    {
        $this->model_class = News::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['create'], $actions['update']);

        return $actions;
    }

    protected function getModelClass()
    {
        return News::className();
    }

    protected function getSearchModelClass()
    {
        return NewsSearch::className();
    }

    public function actionCreate()
    {
        /** @var News $model */
        $model = new News();
        $upload_form = new UploadForm(News::class);

        $model->initDefaultValues();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }

    public function actionUpdate($id)
    {
        /** @var News $model */
        $model = News::findOne($id);
        $upload_form = new UploadForm(News::class);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }
}