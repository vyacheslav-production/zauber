<?php

use yii\grid\GridView;
use app\modules\core\common\models\Regions;

/* @var $this app\modules\core\backend\components\View */
/* @var $searchModel app\modules\credits\backend\models\BcrequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обратная связь';
$this->subTitle = 'Список';
$this->params['breadcrumbs'][] = $this->title;

 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-trash-o"></i> Корзина', 'url' => ['recycle-bin'], 'options' => ['class' => 'btn btn-warning']];

?>
<div class="user-index">
    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'columns' => [
		[                     
	            'label' => 'Id',
	            'attribute' => 'feedback_id',
        	],
		[                     
	            'label' => 'Имя',
	            'attribute' => 'feedback_name',
        	],
		[                     
	            'label' => 'Телефон',
	            'attribute' => 'feedback_phone',
        	],
		[                     
	            'label' => 'E-mail',
	            'attribute' => 'feedback_email',
        	],
		[                     
	            'label' => 'Тема сообщения',
	            'attribute' => 'feedback_topic',
        	],
		[                     
	            'label' => 'Текст сообщения',
	            'attribute' => 'feedback_text',
        	],
		[                     
	            'label' => 'Регион',
	            'value' => function($data) {
	               		return ( Regions::find(['region_code'=>$data->feedback_region])->one()->region_name);
		            },

        	],


            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}']
        ],
    ]); ?>

</div>
