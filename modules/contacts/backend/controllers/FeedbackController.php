<?php

namespace app\modules\contacts\backend\controllers;
use Yii;
use app\modules\contacts\backend\models\FeedbackSearch;
use app\modules\contacts\common\models\Feedback;

use app\modules\core\backend\components\ARController;

class FeedbackController extends ARController
{

    protected function getModelClass()
    {
        return Feedback::className();
    }

    protected function getSearchModelClass()
    {
        return FeedbackSearch::className();
    }

}
