<?php
use app\modules\core\frontend\widgets\MainPageSlider;
use app\modules\core\frontend\widgets\MainPageBanners;
use app\modules\core\frontend\widgets\MainPageUnions;
use app\modules\core\frontend\widgets\MainPageFooter;
use app\modules\core\frontend\widgets\MainPageShownMenu;
use app\modules\core\frontend\widgets\MainPageHiddenMenu;
use app\modules\core\frontend\widgets\MainPageServices;
use app\modules\core\frontend\widgets\MainPageNews;
use app\modules\core\frontend\widgets\MainPageCitySelect;
use app\modules\core\frontend\widgets\Search;
?>

		<div id="wrapper">
			<header id="header">
				<div class="container">
					<div class="contact-panel">
						<strong class="logo"><a href="#">Заубер Банк</a></strong>
			                        <?= MainPageCitySelect::widget(); ?>
						<div class="contacts">
							<a href="tel:+78126470606" class="tel">+7 (812) <strong>647-06-06</strong></a>
							<div class="link-holder">
								<a href="#" class="link">Контактная информация</a>
							</div>
						</div>
						<div class="select-holder pull-right style2 unchanged">
							<select>
								<option>Для физических лиц</option>
								<option>Для юридических лиц</option>
							</select>
						</div>
						<a href="#" class="burger"><span>&nbsp;</span></a>
					</div>
				</div>
				<nav id="nav">
					<div class="container">
						<div class="nav-inner">
							<div class="change-list-holder">
								<ul class="change-list">
									<li class="changetype pli" clientType="private"><a href="#">Частным лицам</a></li>
									<li class="changetype bli" clientType="business"><a href="#">Для бизнеса</a></li>
								</ul>
							</div>
							<div class="menu-holder">
								<a href="#" class="close">X</a>
						                        <?= MainPageShownMenu::widget(); ?>
							</div>
							<div class="drop-holder">
								<a href="#" class="title">Другие услуги</a>
							</div>
				                        <?= Search::widget(); ?>
						</div>
					</div>
					<div class="drop-manu">
						<div class="container hiddenitems">
				                        <?= MainPageHiddenMenu::widget(); ?>
						</div>
					</div>
				</nav>
			</header>
			<div id="main">
				<div class="red-heading">
					<div class="container">
						<h1>Форма обратной связи</h1>
					</div>
				</div>
				<div class="gray-section feedback-section">
					<div class="container">
						<div class="section-inner">
							<div class="section-content">
								<form action="/backend/banners/ajax-send-feedback/" class="default-form feedback-form">
									<fieldset>
										<input type="hidden" name="feedback_region" value="<?= $city ?>">											
										<div class="row inline-row">
											<label>Имя</label>
											<div class="input-holder">
												<input type="text" class="text required" placeholder="Как к вам можно обращаться?" name="feedback_name">
											</div>
										</div>
										<div class="row inline-row">
											<label>Телефон</label>
											<div class="input-holder">
												<input type="tel" class="text phone" placeholder="+ 7 ( ___ ) - ___ - __ - __" name="feedback_phone">
											</div>
										</div>
										<div class="row inline-row">
											<label>E-mail</label>
											<div class="input-holder">
												<input type="text" class="text email" placeholder="Введите ваш электронный адрес" name="feedback_email">
											</div>
										</div>
										<div class="row inline-row">
											<label>Тема сообщения</label>
											<div class="input-holder">
												<select name="feedback_topic">
													<option>Вопрос по кредиту</option>
													<option>Вопрос по депозиту</option>
													<option>Общий вопрос</option>
												</select>
											</div>
										</div>
										<div class="row">
											<label>Описание ситуации</label>
											<textarea placeholder="Введите текст" name="feedback_text"></textarea>
										</div>
										<div class="row submit-row">
											<div class="submit-content">
												<label for="check-agreement">
													<input class="required-check" id="check-agreement" type="checkbox">
												</label>
												<a class="agreement" href="#">Даю согласие на обработку личных данных</a>
											</div>
											<div class="btn-holder">
												<input class="btn btn-red" type="submit" value="Отправить">
											</div>
										</div>
									</fieldset>
								</form>
								<div class="form-sended-holder">
									<div class="middle-holder">
										<div class="middle">
											<span class="ico">&nbsp;</span>
											<h4><strong>Заявка отправлена.</strong></h4>
											<p>Уважаемый <span class="feedname"></span>,</p>
											<p>Спасибо за обращение в Заубер Банк. <br>Заявка отправлена. В ближайшее время с Вами свяжется сотрудник Банка</p>
										</div>
									</div>
								</div>
							</div>
							<div class="aside-widget">&nbsp;</div>
						</div>
					</div>
				</div>
			</div>
                        <?= MainPageFooter::widget(); ?>
		</div>
	</body>
</html>