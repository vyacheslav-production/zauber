<?php

namespace app\modules\contacts\frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\modules\core\common\models\Regions;

class ContactsController extends \yii\web\Controller
{

    public $layout='main.php';

    public function actionIndex($city,$page)
    {
	$city_exist = Regions::find()->andWhere(['region_code'=>$city])->one();
        if ($city_exist === NULL)
	 {
            throw new HttpException(404);
        }
     
            return $this->render($page,[
            'city' => $city,
            'page' => $page,
        ]);

    }
}