<?php

namespace app\modules\contacts\common\models;

use app\modules\core\common\models\Status;
use yii\db\ActiveQuery;

class FeedbackQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}