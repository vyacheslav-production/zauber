<?php
namespace app\modules\contacts\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

use yii\helpers\ArrayHelper;

/**
 * Business credit request form
 */

/**
 * This is the model class for table "feedback".
 *
 * @property integer $feedback_id
 * @property string  $feedback_name
 * @property string  $feedback_phone
 * @property string  $feedback_email
 * @property string  $feedback_topic
 * @property string  $feedback_text
*/

class Feedback extends ActiveRecord
{
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedback_name'], 'string', 'max' => 255],
            [['feedback_phone'], 'string', 'max' => 255],
            [['feedback_email'], 'string', 'max' => 255],
            [['feedback_topic'], 'string', 'max' => 255],
            [['feedback_text'], 'string', 'max' => 65536],
        ];
    }

    public function attributeLabels()
    {
        return [
            'feedback_name'    => 'Имя',
            'feedback_phone'   => 'Телефон',
            'feedback_email'   => 'Электронная почта',
            'feedback_topic'   => 'Тема сообщения',
            'feedback_text'    => 'Текст сообщения',
        ];
    }

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->feedback_id;
    }

    public static function find()
    {
        return new FeedbackQuery(get_called_class());
    }


    public function send()
    {

            $message = "Имя : {$this->feedback_name} <br> Электронная почта: {$this->feedback_email} <br> Телефон: {$this->feedback_phone} <br> Тема сообщения: {$this->feedback_topic} <br> Сообщение: {$this->feedback_text} <br>";

            Yii::$app->mailer->compose()
                ->setTo('f.ustionv@legion.info')
                ->setFrom('info@legion.info')
                ->setSubject('Обратная связь с сайта Zauber')
                ->setHtmlBody($message)
                ->send();

            return true;
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
