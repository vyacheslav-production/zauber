<?php

namespace app\modules\newsmenu\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\newsmenu\common\models\Newsmenu;

/**
 * NewsmenuSearch represents the model behind the search form about `app\modules\newsmenu\common\models\NewsMenu`.
 */
class NewsmenuSearch extends NewsMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Newsmenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $dataProvider;
    }
}
