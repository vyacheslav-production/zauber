<?php

namespace app\modules\newsmenu\backend\controllers;

use app\modules\core\backend\components\ARController;
use Yii;
use app\modules\newsmenu\common\models\Newsmenu;
use app\modules\newsmenu\backend\models\NewsmenuSearch;
use app\modules\core\backend\components\Controller;


class NewsmenuController extends ARController
{

    protected function getModelClass()
    {   
        return NewsMenu::className();
    }

    protected function getSearchModelClass()
    {
        return NewsMenuSearch::className();
    }

}
