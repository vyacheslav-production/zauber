<?php

namespace app\modules\newsmenu\common\models;

use yii\db\ActiveQuery;

class NewsMenuQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}