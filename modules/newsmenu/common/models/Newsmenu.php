<?php

namespace app\modules\newsmenu\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "news_menu".
 *
 * @property integer $news_menu_id
 * @property string  $news_menu_name
 * @property integer $news_menu_order
 */
class NewsMenu extends ActiveRecord
{

    public static function tableName()
    {
        return 'news_menu';
    }
    public function rules()
    {
        return [
            [['news_menu_name'], 'string', 'max' => 255],
            [['news_menu_order'], 'integer', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_menu_id'          => 'ID',
            'news_menu_name'        => 'Название',
            'news_menu_order'       => 'Порядковый номер',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->news_menu_id;
    }

    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new NewsMenuQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }


}
