<?php

namespace app\modules\about\frontend\widgets;

use app\modules\about\common\models\About;
use yii\base\Widget;
use yii\helpers\Url;


class AboutRightMenu extends Widget
{
    public $template = 'right_menu_template';
/*    private function  view_cat($arr,$parent_id = 0) {
        die(print_r($arr));
	if($arr[$parent_id]) {
	return;
	}
	echo '<ul>';
	for($i = 0; $i < count($arr[$parent_id]);$i++) {
		echo '<li><a href="?category_id='.$arr[$parent_id][$i]['id'].'&parent_id='.$parent_id.'">'.$arr[$parent_id][$i]['title'].'</a>';
			view_cat($arr,$arr[$parent_id][$i]['id']);
		echo '</li>';
		}
		echo '</ul>';
	}
*/
    private function ChildFirstLevel($id) {
        $childFirstLevel = About::find()
            ->andWhere(['about_prk_1' => $id ])
            ->andWhere(['about_prk_2' => 0 ])
            ->orderBy('about_name')
            ->all();
       return $childFirstLevel;
    }
    private function ChildSecondLevel($first_id,$second_id) {
        $childFirstLevel = About::find()
            ->andWhere(['about_prk_1' => $first_id ])
            ->andWhere(['about_prk_2' => $second_id ])
            ->orderBy('about_name')
            ->all();
       return $childFirstLevel;
    }

    public function run()
    {
        $imploded = explode("/",Url::to());
        $menu = About::find()
            ->andWhere(['about_prk_1' => 0 ])
            ->andWhere(['about_prk_2' => 0 ])
            ->andWhere(['about_city' => $imploded[1] ])
            ->orderBy('about_name')
            ->all();
         $res = Array();
         foreach ($menu as $item) {
			array_push($res,'<li><a href="/'.$item->about_city.'/about/'.$item->about_title.'/">'.$item->about_name.'</a></li>');
			if (count($this->ChildFirstLevel($item->about_id)))
				foreach ($this->ChildFirstLevel($item->about_id) as $item1)
				       {
					array_push($res,'<li style="margin-left:25px;"><a href="/'.$item1->about_city.'/about/'.$item1->about_title.'/">'.$item1->about_name.'</a></li>');
						if (count($this->ChildSecondLevel($item->about_id,$item1->about_id)))
							foreach ($this->ChildSecondLevel($item->about_id,$item1->about_id) as $item2)
							       {
								array_push($res,'<li style="margin-left:50px;"><a href="/'.$item2->about_city.'/about/'.$item2->about_title.'/">'.$item2->about_name.'</a></li>');
							}	
				       }	
	 }
	 if (count($menu)) { 
            return $this->render($this->template, ['menu' => $res]);
        } else {
            return '';
        }
    }
}