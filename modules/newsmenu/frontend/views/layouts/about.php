<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
$this->registerJsFile('http://maps.google.com/maps/api/js?sensor=false');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=980"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(!empty($this->metaTitle) ? $this->metaTitle : $this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/favicon.png" type="favicon.png">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
</head>
<body>
<?php $this->beginBody() ?>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>


<header id="header">
</header>

<div id="content" class="content">

    <!-- START CONTENT-->

        <?= $content ?>

    <!-- END CONTENT-->

</div>
<footer id="footer">
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
