<?php

namespace app\modules\about\frontend\controllers;

use app\modules\about\common\models\About;
use app\modules\volumes\common\models\Volumes;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

use yii\filters\VerbFilter;

class AboutController extends \yii\web\Controller
{
  public $layout='about.php';

    public function actionIndex($city='spb',$page='general')
    {
//        echo "page= ".$page." city=".$city;
        $cc = About::find()
            ->andWhere(['about_title' => $page ])
            ->andWhere(['about_city' => $city ])
            ->all();
//        die(var_dump($cc));
        $volume = Volumes::find()
            ->andWhere(['volume_title' => About::tableName()])
            ->all();
            return $this->render('index',[
            'context' => $cc,
            'volume_name'=> $volume[0]->volume_name,
            'volume_url'=> '/'.$city.'/'.About::tableName(),
        ]);

    }
}