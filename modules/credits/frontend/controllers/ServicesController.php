<?php

namespace app\modules\services\frontend\controllers;

//use app\modules\news\common\models\Services;
//use app\modules\volumes\common\models\Volumes;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

use yii\filters\VerbFilter;

class ServicesController extends \yii\web\Controller
{
  public $layout='credit.php';

    public function actionIndex($city='spb',$page='credit', $subpage='main';)
    {
     
            return $this->render($page,[
            'city' => $city,
            'subpage' => $subpage,
        ]);

    }
}