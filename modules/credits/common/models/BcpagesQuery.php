<?php

namespace app\modules\credits\common\models;

use app\modules\core\common\models\Status;
use yii\db\ActiveQuery;

class BcpagesQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}