<?php
namespace app\modules\credits\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\modules\core\common\models\Regions;

use yii\helpers\ArrayHelper;

/**
 * Business credit request form
 */

/**
 * This is the model class for table "bcrequest".
 *
 * @property integer $bcrequest_id
 * @property string  $bcrequest_name
 * @property string  $bcrequest_email
 * @property string  $bcrequest_phone
 * @property string  $bcrequest_region
*/

class Bcrequest extends ActiveRecord
{
    public static function tableName()
    {
        return 'bcrequest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bcrequest_name'], 'string', 'max' => 255],
            [['bcrequest_phone'], 'string', 'max' => 255],
            [['bcrequest_email'], 'string', 'max' => 255],
            [['bcrequest_region'], 'string', 'max' => 3],
        ];
    }

    public function attributeLabels()
    {
        return [
            'bcrequest_name'    => 'Имя заявителя',
            'bcrequest_email'   => 'Электронная почта',
            'bcrequest_phone'   => 'Телефон',
            'bcrequest_region'  => 'Город',
        ];
    }

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->bcrequest_id;
    }

    public static function find()
    {
        return new BcrequestQuery(get_called_class());
    }


    public function send()
    {
            $city = Regions::find(['region_code'=>$this->bcrequest_region])->one();
            $message = "Тема: Заявка на бизнес-кредит <br> Имя заявителя: {$this->bcrequest_name} <br> Электронная почта: {$this->bcrequest_email} <br> Телефон: {$this->bcrequest_phone} <br> Город: {$city->region_name}";

            Yii::$app->mailer->compose()
                ->setTo('f.ustionv@legion.info')
                ->setFrom('info@legion.info')
                ->setSubject('Заявка на бизнес-кредит')
                ->setHtmlBody($message)
                ->send();

            return true;
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
