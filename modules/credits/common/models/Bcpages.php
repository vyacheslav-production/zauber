<?php

namespace app\modules\credits\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "bcpages".
 *
 * @property integer $bcpages_id
 * @property string  $bcpages_title
 * @property string  $bcpages_uri
 * @property string  $bcpages_content
 * @property string  $bcpages_region
*/
class Bcpages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bcpages';
    }
    public function rules()
    {
        return [
            [['bcpages_title'], 'string', 'max' => 255],
            [['bcpages_uri'], 'string', 'max' => 255],
            [['bcpages_content'], 'string', 'max' => 65536],
            [['bcpages_region'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bcpages_id'          => 'ID',
            'bcpages_title'       => 'Название',
            'bcpages_uri' 	  => 'URI',
            'bcpages_content' 	  => 'Контент',
            'bcpages_region' 	  => 'Регион',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->bcpages_id;
    }

    /**
     * @inheritdoc
     * @return BcpagesQuery
     */
    public static function find()
    {
        return new BcpagesQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
