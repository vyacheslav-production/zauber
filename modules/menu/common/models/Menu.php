<?php

namespace app\modules\menu\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "mp_menu".
 *
 * @property integer $mp_menu_id
 * @property string  $mp_menu_name
 * @property string  $mp_menu_link
 * @property integer $mp_menu_order
 * @property enum    $mp_menu_type
 */
class Menu extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_menu';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ],
            ],
            'softDelete' => [
                'class' => SoftDeleteBehavior::className()
            ],
        ];
    }
    public function rules()
    {
        return [
            [['mp_menu_name'], 'string', 'max' => 255],
            [['mp_menu_link'], 'string', 'max' => 255],
            [['mp_menu_order'], 'string', 'max' => 2],
            [['mp_menu_type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_menu_id'           => 'ID',
            'mp_menu_name'         => 'Название пункта меню',
            'mp_menu_link'         => 'Ссылка пункта меню',
            'mp_menu_order'        => 'Порядковый номер пункта меню',
            'mp_menu_type'         => 'Тип пункта меню',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current menu item ID
     */
    public function getId()
    {
        return $this->mp_menu_id;
    }

    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
