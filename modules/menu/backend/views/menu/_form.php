<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mp_menu_name')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'mp_menu_link')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'mp_menu_order')->textInput(['maxlength' => 2]) ?>

    <?= $form->field($model, 'mp_menu_type')->textInput(['maxlength' => 30]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
