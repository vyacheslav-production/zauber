<?php

use yii\grid\GridView;

/* @var $this app\modules\core\backend\components\View */
/* @var $searchModel app\modules\user\backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пункты меню';
$this->subTitle = 'Список';
$this->params['breadcrumbs'][] = $this->title;

 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить', 'url' => ['create'], 'options' => ['class' => 'btn btn-success']];
 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-trash-o"></i> Корзина', 'url' => ['recycle-bin'], 'options' => ['class' => 'btn btn-warning']];

?>
<div class="user-index">
    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'columns' => [
		[                     
	            'label' => 'Id',
	            'attribute' => 'id',
        	],
		[                     
	            'label' => 'Название',
	            'attribute' => 'mp_menu_name',
        	],
		[                     
	            'label' => 'Ссылка',
	            'attribute' => 'mp_menu_link',
        	],
		[                     
	            'label' => 'Порядковый номер',
	            'attribute' => 'mp_menu_order',
        	],
		[                     
	            'label' => 'Тип',
	            'attribute' => 'mp_menu_type',
        	],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}',]
        ],
    ]); ?>

</div>
