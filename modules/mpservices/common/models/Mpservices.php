<?php

namespace app\modules\mpservices\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "mp_services".
 *
 * @property integer $mp_services_id
 * @property string  $mp_services_img
 * @property string  $mp_services_link
 * @property string  $mp_services_text
 * @property string  $mp_services_region
 * @property string  $mp_services_type
 */
class Mpservices extends ActiveRecord
{

    static $image_width     = 1200;
    static $image_height    = 600;
    static $limit = 1;
    public $mp_slider_img   = NULL;
    public $mp_banners_img   = NULL;
    public $mp_unions_img   = NULL;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_services';
    }
    public function rules()
    {
        return [
            [['mp_services_img'], 'string', 'max' => 255],
            [['mp_services_link'], 'string', 'max' => 255],
            [['mp_services_text'], 'string', 'max' => 255],
            [['mp_services_region'], 'string', 'max' => 255],
            [['mp_services_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_services_id'        => 'ID',
            'mp_services_img'       => 'Изображение',
            'mp_services_link'      => 'Ссылка',
            'mp_services_text'      => 'Текст',
            'mp_services_region'    => 'Регион',
            'mp_services_type'      => 'Тип',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->mp_services_id;
    }

    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new MpservicesQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
