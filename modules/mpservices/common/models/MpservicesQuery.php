<?php

namespace app\modules\mpservices\common\models;

use app\modules\core\common\models\Status;
use yii\db\ActiveQuery;

class MpservicesQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
//      $this->where(['!=', Menu::tableName() . '.status', Status::ERASED]);
    }

    public function active($state = true)
    {
//        $this->andWhere([Menu::tableName() . '.status' => $state ? Status::ACTIVE : Status::DELETED]);
        return $this;
    }

}