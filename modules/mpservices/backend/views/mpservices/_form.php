<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\services\common\models\Services */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */

echo Html::beginTag('div', ['class' => 'partner-form']);
$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
$items = [
    [
        'label'   => 'Данные',
        'content' => $this->render('_form_common', ['form' => $form, 'model' => $model,'upload_form' => $upload_form]),
        'active'  => true
    ]
];
echo Tabs::widget(['items' => $items]);
echo Html::beginTag('div', ['class' => 'form-group']);
echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
echo Html::endTag('div');
ActiveForm::end();
echo Html::endTag('div');