<?php

use yii\grid\GridView;
use app\modules\core\common\models\Regions;

/* @var $this app\modules\core\backend\components\View */
/* @var $searchModel app\modules\mpservices\backend\models\MpservicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Кнопки услуг';
$this->subTitle = 'Список';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить', 'url' => ['create'], 'options' => ['class' => 'btn btn-success']];
$this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-trash-o"></i> Корзина', 'url' => ['recycle-bin'], 'options' => ['class' => 'btn btn-warning']];

?>
<div class="user-index">
    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'columns' => [
		[                     
	            'label' => 'Id',
	            'attribute' =>'mp_services_id',
        	],
		[                     
	            'label' => 'Изображение',
	            'attribute' => 'mp_services_img',
        	],
		[                     
	            'label' => 'Ссылка',
	            'attribute' => 'mp_services_link',
        	],
		[                     
	            'label' => 'Текст',
	            'attribute' => 'mp_services_text',
        	],
		[                     
	            'label' => 'Регион',
	            'value' => function($data) {
	               		return ( Regions::find(['region_code'=>$data->mp_services_region])->one()->region_name);
		            },

        	],
		[                     
	            'label' => 'Тип',
	            'attribute' => 'mp_services_type',
        	],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}',]
        ],
    ]); ?>

</div>
