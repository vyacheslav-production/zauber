<?php

use yii\helpers\Html;
Use yii\helpers\Url;
use app\modules\core\common\models\Regions;

/* @var $this yii\web\View */
/* @var $model app\modules\partner\common\models\Partner */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */

?>

<br><br>
<?= $form->field($model, 'mp_services_link')->textInput() ?>
<?= $form->field($model, 'mp_services_text')->textInput() ?>
<?= $form->field($model, 'mp_services_region')->dropDownList(Regions::getDropDown()) ?>
<?= $form->field($model, 'mp_services_type')->dropDownList(([
    'private'  => 'Частное лицо',
    'business' => 'Юридическое лицо'
])) ?>

<div class="clearfix">

    <div class="row">
        <div class="col-md-6 contain_img">
            <?= Html::activeHiddenInput($model, 'mp_services_img', ['class' => 'mp_services_img'])?>
            <?= $form->field($upload_form, 'mp_services_img')->fileInput(['class' => 'image_uploads', 'data-field' => 'mp_services_img', 'data-url' => Url::to(['ajax-upload'])]) ?>
            <div class="for_img">
                <?php if($model->mp_services_img): ?>
                    <?= $this->render('@core/views/image/_image', ['model' => $model, 'property' => 'mp_services_img']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>