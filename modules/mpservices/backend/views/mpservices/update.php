<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\common\models\Menu */

$this->title = 'Кнопки услуг';
$this->subTitle = 'Редактирование: '.$model->mp_services_text;

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->subTitle;

$this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-bars"></i> Список', 'url' => ['index'], 'options' => ['class' => 'btn btn-success']];

?>
<div class="user-update">
    <?= $this->render('_form', [
        'model' => $model,
        'upload_form' => $upload_form,
    ]) ?>

</div>
