<?php

namespace app\modules\mpservices\backend\controllers;
use Yii;
use app\modules\mpservices\backend\models\MpservicesSearch;
use app\modules\mpservices\common\models\Mpservices;
use app\modules\core\backend\components\UploadForm;

use app\modules\core\backend\components\ARController;

class MpservicesController extends ARController
{

    public function beforeAction($action)
    {
        $this->model_class = Mpservices::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['create'], $actions['update']);

        return $actions;
    }

    protected function getModelClass()
    {
        return Mpservices::className();
    }

    protected function getSearchModelClass()
    {          
        return MpservicesSearch::className();
    }
    public function actionUpdate($id)
    {
        /** @var Mpservices $model */
        $model = Mpservices::findOne($id);
        $upload_form = new UploadForm(Mpservices::class);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } 

        
        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }
    public function actionCreate()
    {
        /** @var Mpservices $model */
        $model = new Mpservices;
        $upload_form = new UploadForm(Mpservices::class);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } 

        
        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }


}
