<?php

namespace app\modules\deposits\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\deposits\common\models\Dpslider;

/**
 * DpsliderSearch represents the model behind the search form about `app\modules\dpslider\common\models\Dpslider`.
 */
class DpsliderSearch extends Dpslider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dpslider::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $dataProvider;
    }
}
