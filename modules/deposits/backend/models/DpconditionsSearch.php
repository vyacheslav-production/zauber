<?php

namespace app\modules\deposits\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\deposits\common\models\Dpconditions;

/**
 * DpconditionsSearch represents the model behind the search form about `app\modules\dpconditions\common\models\Dpconditions`.
 */
class DpconditionsSearch extends Dpconditions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dpconditions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $dataProvider;
    }
}
