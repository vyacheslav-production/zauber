<?php

namespace app\modules\deposits\backend\controllers;
use Yii;
use app\modules\deposits\backend\models\DppagesSearch;
use app\modules\deposits\common\models\Dppages;

use app\modules\core\backend\components\ARController;

class DppagesController extends ARController
{

    protected function getModelClass()
    {
        return Dppages::className();
    }

    protected function getSearchModelClass()
    {
        return DppagesSearch::className();
    }

}
