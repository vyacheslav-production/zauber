<?php

namespace app\modules\deposits\backend\controllers;
use Yii;
use app\modules\deposits\backend\models\DpsliderSearch;
use app\modules\deposits\common\models\Dpslider;
use app\modules\core\backend\components\UploadForm;

use app\modules\core\backend\components\ARController;

class DpsliderController extends ARController
{

    public function beforeAction($action)
    {
        $this->model_class = Dpslider::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['update'],$actions['create']);

        return $actions;
    }

    protected function getModelClass()
    {
        return Dpslider::className();
    }

    protected function getSearchModelClass()
    {          
        return DpsliderSearch::className();
    }
    public function actionUpdate($id)
    {
        /** @var Dpslider $model */
        $model = Dpslider::findOne($id);
        $upload_form = new UploadForm(Dpslider::class);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } 

        
        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }
    public function actionCreate()
    {
	$model = new Dpslider();
        $upload_form = new UploadForm(Dpslider::class);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } 

        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);

    }

}
