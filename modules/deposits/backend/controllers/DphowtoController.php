<?php

namespace app\modules\deposits\backend\controllers;

use app\modules\core\backend\components\ARController;
use Yii;
use app\modules\deposits\common\models\Dphowto;
use app\modules\deposits\backend\models\DphowtoSearch;
use app\modules\core\backend\components\Controller;

class DphowtoController extends ARController
{

    protected function getModelClass()
    {   
        return Dphowto::className();
    }

    protected function getSearchModelClass()
    {
        return DphowtoSearch::className();
    }


}
