<?php

namespace app\modules\deposits\backend\controllers;
use Yii;
use app\modules\deposits\backend\models\DptypesSearch;
use app\modules\deposits\common\models\Dptypes;
use app\modules\core\backend\components\UploadForm;
use yii\helpers\ArrayHelper;

use app\modules\core\backend\components\ARController;

class DptypesController extends ARController
{

    public function beforeAction($action)
    {
        $this->model_class = Dptypes::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['create'], $actions['update']);

        return $actions;
    }

    protected function getModelClass()
    {
        return Dptypes::className();
    }

    protected function getSearchModelClass()
    {          
        return DptypesSearch::className();
    }
    public function actionUpdate($id)
    {
        /** @var Dptypes $model */
        $model = Dptypes::findOne($id);
        $upload_form = new UploadForm(Dptypes::class);

        if (Yii::$app->request->isPost) {


        $ret = [];
	if (ArrayHelper::getValue(Yii::$app->request->post(), 'acblock_pct') !== NULL)
	{
	        $pct =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'acblock_pct'));
	        $txt =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'acblock_txt'));
	        $ret =[];
	        foreach ($pct as $key=>$value)
	        {	
	              $arr = [];
	              $arr['picture'] = $value;
	              $arr['text'] = $txt[$key];
	              array_push($ret,$arr);
	        }
	}

	$model->load(Yii::$app->request->post());
	$model->dptypes_block = serialize($ret);
        if ($model->save()) {
            return $this->redirect(['index']);
        } 

      }  
        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }
    public function actionCreate()
    {
        /** @var Dptypes $model */
        $model = new Dptypes();
        $upload_form = new UploadForm(Dptypes::class);

        if (Yii::$app->request->isPost) {


        $ret = [];
	if (ArrayHelper::getValue(Yii::$app->request->post(), 'acblock_pct') !== NULL)
	{
	        $pct =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'acblock_pct'));
	        $txt =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'acblock_txt'));
	        $ret =[];
	        foreach ($pct as $key=>$value)
	        {	
	              $arr = [];
	              $arr['picture'] = $value;
	              $arr['text'] = $txt[$key];
	              array_push($ret,$arr);
	        }
	}

	$model->load(Yii::$app->request->post());
	$model->dptypes_block = serialize($ret);
        if ($model->save()) {
            return $this->redirect(['index']);
        } 

      }  
        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }


}
