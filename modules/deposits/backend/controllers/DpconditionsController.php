<?php

namespace app\modules\deposits\backend\controllers;

use app\modules\core\backend\components\ARController;
use Yii;
use app\modules\deposits\common\models\Dpconditions;
use app\modules\deposits\backend\models\DpconditionsSearch;
use app\modules\core\backend\components\Controller;
use app\modules\core\backend\components\UploadForm;

class DpconditionsController extends ARController
{

    protected function getModelClass()
    {   
        return Dpconditions::className();
    }

    protected function getSearchModelClass()
    {
        return DpconditionsSearch::className();
    }

    public function beforeAction($action)
    {
        $this->model_class = Dpconditions::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['create'], $actions['update']);

        return $actions;
    }


    public function actionUpdate($id)
    {
        /** @var Dpconditions $model */
        $model = Dpconditions::findOne($id);

        $upload_form = new UploadForm(Dpconditions::class);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } 

        
        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);
    }

    public function actionCreate()
    {
	$model = new Dpconditions();
        $upload_form = new UploadForm(Dpconditions::class);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } 

        return $this->render('update', [
            'model' => $model,
            'upload_form' => $upload_form
        ]);

    }

}
