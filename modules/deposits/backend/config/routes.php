<?php

return [
    'deposits/dpslider/<action:[\w\-]+>' => '/deposits/dpslider/<action>',
    'deposits/dphowto/<action:[\w\-]+>' => '/deposits/dphowto/<action>',
    'deposits/dpconditions/<action:[\w\-]+>' => '/deposits/dpconditions/<action>',
    'deposits/dptypes/<action:[\w\-]+>' => '/deposits/dptypes/<action>',
    'deposits/dppages/<action:[\w\-]+>' => '/deposits/dppages/<action>',
];