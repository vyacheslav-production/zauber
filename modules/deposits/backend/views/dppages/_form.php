<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use app\modules\core\common\models\Regions;

/* @var $this yii\web\View */
/* @var $model app\modules\user\common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dppages_title')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'dppages_uri')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'dppages_region')->dropDownList((
                Regions::getDropDown()
    )) ?>

    <?= $form->field($model, 'dppages_content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced'
    ]) ?>



    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
