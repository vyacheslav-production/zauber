<?php

use yii\grid\GridView;

/* @var $this app\modules\core\backend\components\View */
/* @var $searchModel app\modules\credits\backend\models\dppagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы шаблона "Кредиты"';
$this->subTitle = 'Список';
$this->params['breadcrumbs'][] = $this->title;

 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить', 'url' => ['create'], 'options' => ['class' => 'btn btn-success']];
 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-trash-o"></i> Корзина', 'url' => ['recycle-bin'], 'options' => ['class' => 'btn btn-warning']];

?>
<div class="user-index">
    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'columns' => [
		[                     
	            'label' => 'Id',
	            'attribute' => 'dppages_id',
        	],
		[                     
	            'label' => 'Название',
	            'attribute' => 'dppages_title',
        	],
		[                     
	            'label' => 'URI',
	            'attribute' => 'dppages_uri',
        	],
		[                     
	            'label' => 'Контент',
	            'attribute' => 'dppages_content',
        	],
		[                     
	            'label' => 'Регион',
	            'attribute' => 'dppages_region',
        	],


            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}']
        ],
    ]); ?>

</div>
