<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use app\modules\core\common\models\Regions;

use app\modules\about\common\models\About;

/* @var $this yii\web\View */
/* @var $model app\modules\about\common\models\About */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'dphowto_title1')->textInput() ?>
    <?= $form->field($model, 'dphowto_description1')->textInput() ?>
    <?= $form->field($model, 'dphowto_title2')->textInput() ?>
    <?= $form->field($model, 'dphowto_description2')->textInput() ?>
    <?= $form->field($model, 'dphowto_title3')->textInput() ?>
    <?= $form->field($model, 'dphowto_description3')->textInput() ?>
    <?= $form->field($model, 'dphowto_title4')->textInput() ?>
    <?= $form->field($model, 'dphowto_description4')->textInput() ?>
    <?= $form->field($model, 'dphowto_page')->textInput() ?>
    <?= $form->field($model, 'dphowto_region')->dropDownList((
                Regions::getDropDown()
    )) ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
