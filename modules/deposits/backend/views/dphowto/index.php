<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\about\backend\models\About */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Как открыть вклад';
$this->subTitle                = 'Список';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить',
                           'url'     => ['create'],
                           'options' => ['class' => 'btn btn-success']
];
$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-trash-o"></i> Корзина',
                           'url'     => ['recycle-bin'],
                           'options' => ['class' => 'btn btn-warning']
];
?>
<div class="program-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
		[                     
	            'label' => 'Id',
	            'attribute' =>'dphowto_id',
        	],
		[                     
	            'label' => 'Заголовок 1',
	            'attribute' => 'dphowto_title1',
        	],
		[                     
	            'label' => 'Описание 1',
	            'attribute' => 'dphowto_description1',
        	],
		[                     
	            'label' => 'Заголовок 2',
	            'attribute' => 'dphowto_title2',
        	],
		[                     
	            'label' => 'Описание 2',
	            'attribute' => 'dphowto_description2',
        	],
		[                     
	            'label' => 'Заголовок 3',
	            'attribute' => 'dphowto_title3',
        	],
		[                     
	            'label' => 'Описание 3',
	            'attribute' => 'dphowto_description3',
        	],
		[                     
	            'label' => 'Заголовок 4',
	            'attribute' => 'dphowto_title4',
        	],
		[                     
	            'label' => 'Описание 4',
	            'attribute' => 'dphowto_description4',
        	],

		[                     
	            'label' => 'Страница',
	            'attribute' => 'dphowto_page',
        	],
		[                     
	            'label' => 'Регион',
	            'attribute' => 'dphowto_region',
        	],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>

</div>
