<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\deposits\common\models\Dpconditions */

$this->title = 'Баннер кредита для бизнеса';
$this->subTitle = 'Редактирование: '.$model->dpconditions_title;

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->subTitle;

$this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-bars"></i> Список', 'url' => ['index'], 'options' => ['class' => 'btn btn-success']];

?>
<div class="user-update">
    <?= $this->render('_form', [
        'model' => $model,
        'upload_form' => $upload_form,
    ]) ?>

</div>
