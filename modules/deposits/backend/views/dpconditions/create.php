<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\dpconditions\common\models\Dpconditions */

$this->title = 'Преимущества кредита для бизнеса';
$this->subTitle = 'Добавить';

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->subTitle;
?>
<div class="program-create">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>