<?php

use yii\grid\GridView;
use app\modules\core\common\models\Regions;

/* @var $this app\modules\core\backend\components\View */
/* @var $searchModel app\modules\credits\backend\models\BcbannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Условия раздела "Вклады"';
$this->subTitle = 'Список';
$this->params['breadcrumbs'][] = $this->title;

 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить', 'url' => ['create'], 'options' => ['class' => 'btn btn-success']];
 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-trash-o"></i> Корзина', 'url' => ['recycle-bin'], 'options' => ['class' => 'btn btn-warning']];

?>
<div class="user-index">
    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'columns' => [
		[                     
	            'label' => 'Id',
	            'attribute' =>'dpconditions_id',
        	],
		[                     
	            'label' => 'Название',
	            'attribute' => 'dpconditions_title',
	       	],
		[                     
	            'label' => 'Текст',
	            'attribute' => 'dpconditions_text',
        	],
		[                     
	            'label' => 'Изображение',
	            'attribute' => 'dpconditions_img',
        	],
		[                     
	            'label' => 'Страница',
	            'attribute' => 'dpconditions_page',
        	],
		[                     
	            'label' => 'Регион',
	            'value' => function($data) {
	               		return ( Regions::find(['region_code'=>$data->dpconditions_region])->one()->region_name);
		            },

        	],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}',]
        ],
    ]); ?>

</div>
