<?php

use yii\helpers\Html;
Use yii\helpers\Url;
use app\modules\core\common\models\Regions;

/* @var $this yii\web\View */
/* @var $model app\modules\dpconditions\common\models\Dpconditions */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */

?>

<br><br>
<?= $form->field($model, 'dpconditions_title')->textInput() ?>
<?= $form->field($model, 'dpconditions_text')->textInput() ?>
<?= $form->field($model, 'dpconditions_page')->textInput() ?>
<?= $form->field($model, 'dpconditions_region')->dropDownList(Regions::getDropDown()) ?>
<div class="clearfix">

    <div class="row">
        <div class="col-md-6 contain_img">
            <?= Html::activeHiddenInput($model, 'dpconditions_img', ['class' => 'dpconditions_img'])?>
            <?= $form->field($upload_form, 'dpconditions_img')->fileInput(['class' => 'image_uploads', 'data-field' => 'dpconditions_img', 'data-url' => Url::to(['ajax-upload'])]) ?>
            <div class="for_img">
                <?php if($model->dpconditions_img): ?>
                    <?= $this->render('@core/views/image/_image', ['model' => $model, 'property' => 'dpconditions_img']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>