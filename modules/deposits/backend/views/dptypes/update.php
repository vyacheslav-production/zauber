<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\about\common\models\About */

$this->title = 'Преимущества раздела "Автокредитование"';
$this->subTitle = 'Редактирование';

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-bars"></i> Список', 'url' => ['index'], 'options' => ['class' => 'btn btn-success']];

?>
<div class="program-update">
    <?= $this->render('_form', ['model' => $model,        
				'upload_form' => $upload_form,]) ?>

</div>
