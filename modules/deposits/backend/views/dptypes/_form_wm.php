<?php

use yii\helpers\Html;
Use yii\helpers\Url;

?>
<div class="unfold">
<?php   if ($model->dptypes_block !='')
	foreach (unserialize($model->dptypes_block) as $key=>$value) {?>
	<label class="control-label <?php echo 'picture'.$key?>">Изображение в блоке картинок</label>
		<?php echo Html::Input('hidden','picture[]', $value['picture'],['class'=>'picture'.$key, 'id'=>'pct_name'.$key]) ?>
   				<?php echo Html::beginForm('','post',['id'=>'wmform','class'=>'form_upload_wm','action'=>'','id'=>$key,'enctype'=>'multipart/form-data']) ?>
				<?php echo Html::fileInput('fi','',['class'=>'image_uploads_wm','id'=>'fi','data-url'=>Url::to(['ajax-upload-wm'])]) ?>
   				<?php echo Html::endForm() ?>
		<?= $this->render('@core/views/image/_image_wm',['img_src' => $value['picture'],'img_id'=>'pct'.$key]) ?>
	<br>
	<label class="control-label <?php echo 'text'.$key?>">Текст в блоке картинок</label>
		<?php echo Html::input('text','text[]',$value['text'],['class'=>'text'.$key]) ?>
	<br><br>
        <?php echo Html::Button('Удалить', ['class' => 'btn btn-primary delacadv','id' => $key ]);?>
	<br><br>
<?php }?>
</div>
	<br><br>
        <?php echo Html::Button('Добавить', ['class' => 'btn btn-primary addacadv' ]);?>
	<br><br>

