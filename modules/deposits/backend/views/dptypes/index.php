<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\deposits\backend\models\Dptypes */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Типы вкладов';
$this->subTitle                = 'Список';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить',
                           'url'     => ['create'],
                           'options' => ['class' => 'btn btn-success']
];
$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-trash-o"></i> Корзина',
                           'url'     => ['recycle-bin'],
                           'options' => ['class' => 'btn btn-warning']
];
?>
<div class="program-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
		[                     
	            'label' => 'Id',
	            'attribute' =>'dptypes_id',
        	],
		[                     
	            'label' => 'Блок с картинками',
	            'attribute' => 'dptypes_block',
	            'value' => function($data) {
	               		return ( $this->getBlockProperties($data->dptypes_block));
		            },

        	],
		[                     
	            'label' => 'Заголовок',
	            'attribute' => 'dptypes_header',
        	],
		[                     
	            'label' => 'Описание',
	            'attribute' => 'dptypes_description',
        	],
		[                     
	            'label' => 'Изображение',
	            'attribute' => 'dptypes_img',
        	],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>

</div>
