<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\services\common\models\Services */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */

echo Html::beginTag('div');
echo $this->render('_form_wm', ['model' => $model]);
$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
echo $this->render('_form_common', ['form' => $form, 'model' => $model,'upload_form' => $upload_form]);
echo Html::beginTag('div', ['class' => 'form-group']);
//echo Html::Button('Сохранить', ['class' => 'btn btn-primary advfold']);
echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary advfold' ]);

echo Html::endTag('div');
ActiveForm::end();
