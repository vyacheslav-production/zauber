<?php

use yii\helpers\Html;
Use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\dptypes\common\models\Dptypes */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */
/* */
?>
<?= $form->field($model, 'dptypes_header')->textInput() ?>
<?= $form->field($model, 'dptypes_description')->textInput() ?>
<?php echo Html::Input('hidden','acblock_pct','',['id'=>'acblock_pct']) ?> 
<?php echo Html::Input('hidden','acblock_txt','',['id'=>'acblock_txt']) ?> 
<br><br>
<div class="clearfix">

    <div class="row">
        <div class="col-md-6 contain_img">
            <?= Html::activeHiddenInput($model, 'dptypes_img', ['class' => 'dptypes_img'])?>
            <?= $form->field($upload_form, 'dptypes_img')->fileInput(['class' => 'image_uploads', 'data-field' => 'dptypes_img', 'data-url' => Url::to(['ajax-upload'])]) ?>
            <div class="for_img">
                <?php if($model->dptypes_img): ?>
                    <?= $this->render('@core/views/image/_image', ['model' => $model, 'property' => 'dptypes_img']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>