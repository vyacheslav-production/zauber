<?php

use yii\helpers\Html;
Use yii\helpers\Url;
use app\modules\core\common\models\Regions;

/* @var $this yii\web\View */
/* @var $model app\modules\dpslider\common\models\Dpslider */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */

?>

<br><br>
<?= $form->field($model, 'dpslider_header')->textInput() ?>
<?= $form->field($model, 'dpslider_description')->textInput() ?>
<?= $form->field($model, 'dpslider_page')->textInput() ?>
<?= $form->field($model, 'dpslider_deposit_id')->textInput() ?>
<?= $form->field($model, 'dpslider_region')->dropDownList(Regions::getDropDown()) ?>
<div class="clearfix">

    <div class="row">
        <div class="col-md-6 contain_img">
            <?= Html::activeHiddenInput($model, 'dpslider_img', ['class' => 'dpslider_img'])?>
            <?= $form->field($upload_form, 'dpslider_img')->fileInput(['class' => 'image_uploads', 'data-field' => 'dpslider_img', 'data-url' => Url::to(['ajax-upload'])]) ?>
            <div class="for_img">
                <?php if($model->dpslider_img): ?>
                    <?= $this->render('@core/views/image/_image', ['model' => $model, 'property' => 'dpslider_img']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>