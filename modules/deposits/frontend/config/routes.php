<?php

return [
    '/<city:\w+>/deposits/<page:\w+>/<subpage:\w+>/' => '/deposits/deposits/index/',
    '/<city:\w+>/deposits/<page:\w+>/' => '/deposits/deposits/index/',
    '/<city:\w+>/deposits/' => '/deposits/deposits/index/',
    'search/'      => 'core/site/search',
];