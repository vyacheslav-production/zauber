<?php
use app\modules\core\frontend\widgets\MainPageSlider;
use app\modules\core\frontend\widgets\MainPageBanners;
use app\modules\core\frontend\widgets\MainPageUnions;
use app\modules\core\frontend\widgets\MainPageFooter;
use app\modules\core\frontend\widgets\MainPageShownMenu;
use app\modules\core\frontend\widgets\MainPageHiddenMenu;
use app\modules\core\frontend\widgets\MainPageServices;
use app\modules\core\frontend\widgets\MainPageNews;
use app\modules\core\frontend\widgets\MainPageCitySelect;
use app\modules\core\frontend\widgets\Search;
use app\modules\deposits\frontend\widgets\DepositSlider;
use app\modules\deposits\frontend\widgets\DepositConditions;
use app\modules\deposits\frontend\widgets\DepositTypes;
use app\modules\deposits\frontend\widgets\DepositHowto;
?>
		<div id="wrapper">
			<header id="header">
				<div class="container">
					<div class="contact-panel">
						<strong class="logo"><a href="/">Заубер Банк</a></strong>
			                        <?= MainPageCitySelect::widget(); ?>
						<div class="contacts">
							<a href="tel:+78126470606" class="tel">+7 (812) <strong>647-06-06</strong></a>
							<div class="link-holder">
								<a href="#" class="link">Контактная информация</a>
							</div>
						</div>
						<div class="select-holder pull-right style2 unchanged">
							<select>
								<option>Для физических лиц</option>
								<option>Для юридических лиц</option>
							</select>
						</div>
						<a href="#" class="burger"><span>&nbsp;</span></a>
					</div>
				</div>
				<nav id="nav">
					<div class="container">
						<div class="nav-inner">
							<div class="change-list-holder">
								<ul class="change-list">
									<li class="changetype pli" clientType="private"><a href="#">Частным лицам</a></li>
									<li class="changetype bli" clientType="business"><a href="#">Для бизнеса</a></li>
								</ul>
							</div>
							<div class="menu-holder">
								<a href="#" class="close">X</a>
						                        <?= MainPageShownMenu::widget(); ?>
							</div>
							<div class="drop-holder">
								<a href="#" class="title">Другие услуги</a>
							</div>
				                        <?= Search::widget(); ?>
						</div>
					</div>
					<div class="drop-manu">
						<div class="container hiddenitems">
				                        <?= MainPageHiddenMenu::widget(); ?>
						</div>
					</div>
				</nav>
			</header>
			<div id="main">
				<div class="top-menu-holder">
					<div class="container">
						<ul class="top-menu">
							<li class="active"><a href="#">Кредиты для СМБ</a></li>
							<li><a href="#">Инвестиционный кредит</a></li>
							<li><a href="#">Кредитная линия</a></li>
							<li><a href="#">Овердафт</a></li>
							<li><a href="#">Тендерный кредит</a></li>
							<li><a href="#">Банковская гарантия</a></li>
						</ul>
					</div>
				</div>
				<div class="intro type2">
					<ul class="breadcrumbs">
						<li><a href="#">Для бизнеса</a></li>
						<li class="active"><a href="#">Вклады</a></li>
					</ul>
					<?= DepositSlider::widget();?>
					<?= DepositConditions::widget();?>
				</div>
				<div class="advantages-section type2">
				         <?= DepositTypes::widget();?>
				</div>
				<div class="contribution-section">
					<div class="container">
						<h2>Как открыть вклад</h2>
					   <?= DepositHowto::widget() ?> 	
					</div>
				</div>
				<div class="dep-section">
					<div class="container">
						<form action="deposit.php" class="default-form deposit-form">
							<fieldset>
								<div class="inner">
									<div class="column">
										<h2>Подобрать выгодный вклад</h2>
										<div class="radio-holder first">
											<span class="title">Валюта</span>
											<ul class="radio-list">
												<li>
													<label for="radio-1">
														<input id="radio-1" name="radio-group" type="radio" value="российский Рубль, ₽" checked>
														Российский Рубль, ₽
													</label>
												</li>
												<li>
													<label for="radio-2">
														<input id="radio-2" name="radio-group" type="radio" value="доллар США, $">
														Доллар США, $
													</label>
												</li>
												<li>
													<label for="radio-3">
														<input id="radio-3" name="radio-group" type="radio" value="евро, €">
														Eвро, €
													</label>
												</li>
											</ul>
										</div>
										<div class="range-slider">
											<div class="heading">
												<span class="title">Сумма</span>
												<span class="result">1 000 000</span>
											</div>
											<div class="slider-range">
												<span class="line">&nbsp;</span>
												<input class="min_max_currentmin_currentmax" type="text" value="0/2000000/1000000"> <!-- min value/max value/current value -->
												<input type="text" class="hide" name="sum">
											</div>
										</div>
										<div class="radio-holder type2">
											<span class="title">Период</span>
											<ul class="radio-list">
												<li>
													<label for="radio-4">
														<input id="radio-4" name="radio-group2" type="radio" value="3 мес." checked>
														3 мес.
													</label>
												</li>
												<li>
													<label for="radio-5">
														<input id="radio-5" name="radio-group2" type="radio" value="6 мес.">
														6 мес.
													</label>
												</li>
												<li>
													<label for="radio-6">
														<input id="radio-6" name="radio-group2" type="radio" value="12 мес.">
														12 мес.
													</label>
												</li>
												<li>
													<label for="radio-7">
														<input id="radio-7" name="radio-group2" type="radio" value="24 мес.">
														24 мес.
													</label>
												</li>
												<li>
													<label for="radio-8">
														<input id="radio-8" name="radio-group2" type="radio" value="36 мес.">
														36 мес.
													</label>
												</li>
											</ul>
										</div>
										<div class="range-slider">
											<div class="heading">
												<span class="title">Ежемесячное пополнение</span>
												<span class="result">20 000</span>
											</div>
											<div class="slider-range">
												<span class="line">&nbsp;</span>
												<input class="min_max_currentmin_currentmax" type="text" value="0/100000/20000"> <!-- min value/max value/current value -->
												<input type="text" class="hide" name="refill">
											</div>
										</div>
										<ul class="check-list">
											<li>
												<label for="check-1">
													<input id="check-1" type="checkbox" value="да" name="check-1" checked>
													Капитализация процентов
												</label>
											</li>
											<li>
												<label for="check-2">
													<input id="check-2" type="checkbox" value="да" name="check-2">
													Досрочное снятие
												</label>
											</li>
										</ul>
										<span class="info-text">Расчеты являются предварительными <br>и не являются публичной офертой</span>
									</div>
									<div class="column" id="credit-selected">
										<span class="info">*Доступные вклады</span>
										<ul class="dep-list onLoad">
											<li class="active">
												<h2>Чистая выгода</h2>
												<div class="content">
													<div class="description">
														<dl>
															<dt>Ставка</dt>
															<dd>7%</dd>
														</dl>
														<dl>
															<dt>Период</dt>
															<dd>91 день</dd>
														</dl>
														<dl>
															<dt>Доход</dt>
															<dd>+ 12 652 ₽</dd>
														</dl>
													</div>
													<div class="total">
														<span class="title">Итого</span>
														<span class="price">1 032 652 <span class="rub">a</span></span>
													</div>
												</div>
											</li>
											<li>
												<h2>VIP-Классический</h2>
												<div class="content">
													<div class="description">
														<dl>
															<dt>Ставка</dt>
															<dd>7%</dd>
														</dl>
														<dl>
															<dt>Период</dt>
															<dd>91 день</dd>
														</dl>
														<dl>
															<dt>Доход</dt>
															<dd>+ 12 652 ₽</dd>
														</dl>
													</div>
													<div class="total">
														<span class="title">Итого</span>
														<span class="price">1 032 652 <span class="rub">a</span></span>
													</div>
												</div>
											</li>
											<li>
												<h2>Уверенная стабильность</h2>
												<div class="content">
													<div class="description">
														<dl>
															<dt>Ставка</dt>
															<dd>7%</dd>
														</dl>
														<dl>
															<dt>Период</dt>
															<dd>91 день</dd>
														</dl>
														<dl>
															<dt>Доход</dt>
															<dd>+ 12 652 ₽</dd>
														</dl>
													</div>
													<div class="total">
														<span class="title">Итого</span>
														<span class="price">1 032 652 <span class="rub">a</span></span>
													</div>
												</div>
											</li>
											<li>
												<h2>Привилегия</h2>
												<div class="content">
													<div class="description">
														<dl>
															<dt>Ставка</dt>
															<dd>7%</dd>
														</dl>
														<dl>
															<dt>Период</dt>
															<dd>91 день</dd>
														</dl>
														<dl>
															<dt>Доход</dt>
															<dd>+ 12 652 ₽</dd>
														</dl>
													</div>
													<div class="total">
														<span class="title">Итого</span>
														<span class="price">1 032 652 <span class="rub">b</span></span>
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div class="column">
										<h2>Контактные данные</h2>
										<span class="selected-info">Выбранный вклад 
											<span class="select-holder">
												<select name="selected-deposit">
													<option selected>Чистая выгода</option>
													<option>VIP-Классический</option>
													<option>Уверенная стабильность</option>
													<option>Привилегия</option>
												</select>
											</span>
										</span>
										<div class="col-row">
											<div class="col l-12 t-6 m-12">
												<div class="row">
													<label>Ваше Ф.И.О.:</label>
													<input type="text" class="text required" placeholder="Иванов Иван Иванович" name="username">
												</div>
												<div class="row">
													<label>Телефон</label>
													<div class="short-input">
														<input type="tel" class="text phone" placeholder="+ 7 ( ___ ) - ___ - __ - __" name="phone">
													</div>
												</div>
												<div class="row">
													<label>E-mail </label>
													<div class="short-input">
														<input type="text" class="text email" name="email">
													</div>
												</div>
											</div>
											<div class="col l-12 t-6 m-12">
												<div class="row">
													<label>Регион постоянной регистрации</label>
													<div class="select-holder">
														<select name="post-reg">
															<option>Москва</option>
															<option selected>Санкт-Петербург</option>
															<option>Черкесск</option>
															<option>Зеленчук</option>
														</select>
													</div>
												</div>
												<div class="row">
													<label>Регион фактического проживания</label>
													<div class="select-holder">
														<select name="fact-reg">
															<option>Москва</option>
															<option selected>Санкт-Петербург</option>
															<option>Черкесск</option>
															<option>Зеленчук</option>
														</select>
													</div>
												</div>
												<div class="row submit-row">
													<input class="btn btn-red" type="submit" value="Открыть вклад">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-sended-holder">
									<div class="middle-holder">
										<div class="middle">
											<span class="ico">&nbsp;</span>
											<h4><strong>Заявка отправлена.</strong></h4>
											<p>Уважаемый <span class="feedname">Иван Иванович</span>,</p>
											<p>Спасибо за обращение в Заубер Банк. <br>Ваша онлайн заявка на вклад принята.</p>
											<p><strong>В ближайшее время на вашу электронну. почту придет письмо <br>с необходимой информацией для дальнейших действий.</strong></p>
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<footer id="footer">
				<div class="inner">
					<div class="container">
						<div class="top-holder">
							<div class="license-holder">
								<span class="title">Лицензия ЦБ РФ на осуществление банковских операций № 1614</span>
								<p>Банк включён в реестр банков-участников системы страхования вкладов <br>под номером 122</p>
							</div>
							<div class="controls">
								<div class="social-holder">
									<div class="item">
										<span class="title">Присоединяйтесь</span>
										<ul class="socials">
											<li><a class="facebook" href="#">facebook</a></li>
											<li><a class="vkontakte" href="#">vkontakte</a></li>
										</ul>
									</div>
									<div class="item">
										<span class="title">Создание сайта</span>
										<div class="visual"><a href="#"><img src="images/ico2.png" alt="image description"></a></div>
									</div>
								</div>
								<div class="footer-list-holder">
									<ul class="footer-list">
										<li><a href="#">О банке</a></li>
										<li><a href="#">Обратная связь</a></li>
										<li><a href="#">Контакты</a></li>
										<li><a href="#">Новости</a></li>
										<li><a href="#">Курсы валют</a></li>
										<li><a href="#">Вакансии</a></li>
										<li><a href="#">Реквизиты</a></li>
										<li><a href="#">Услуги</a></li>
										<li><a href="#">Интернет-банк</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="copy-panel">
							<div class="copy-list-holder">
								<ul class="copy-list">
									<li><a href="#">Список лиц, под контролем либо значительным влиянием которых находится банк</a></li>
									<li><a href="#">Информация о процентных ставках по договорам банковского вклада с физическими лицами</a></li>
								</ul>
							</div>
							<div class="copy-holder">
								<span class="copyright">&copy; 1992-2016 АО «<a href="#">Заубер Банк</a>». <br>Все права защищены</span>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</body>
</html>