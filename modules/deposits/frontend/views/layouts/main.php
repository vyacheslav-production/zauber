<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\modules\attributes\common\models\Attributes;

AppAsset::register($this);

$model = Attributes::find()->where(['attributes_page' => Url::to()])->one();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php if (isset($model)) {?>
    <meta name="keywords" content="<?= $model->attributes_keywords ?>">
    <meta name="description" content="<?= $model->attributes_description ?>">
<?php } ?>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
<?php if (isset($model)) {?>
    <title><?= $model->attributes_title ?></title>
<?php } else {?>
    <title>Заголовок не задан</title>
<?php } ?>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>
        <?= $content ?>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
