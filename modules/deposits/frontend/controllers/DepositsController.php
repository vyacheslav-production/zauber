<?php

namespace app\modules\deposits\frontend\controllers;

use app\modules\core\common\models\Regions;
use app\modules\deposits\common\models\Dppages;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\HttpException;

use yii\filters\VerbFilter;

class DepositsController extends \yii\web\Controller
{
  public $layout='main.php';

    public function actionIndex($city='',$page='',$subpage='')
    {
	$city_exist = Regions::find()->andWhere(['region_code'=>$city])->one();
        if ($city_exist === NULL)
	 {
            throw new HttpException(404);
        }
            $content =Dppages::find()->andWhere(['dppages_uri'=>$page])->one();
            if ($content === NULL)
            {
	            $content =Dppages::find()->andWhere(['dppages_uri'=>'main'])->one();
	            return $this->render('index',[
	            'content' =>$content,
	            'city' => $city,
	            'subpage' => $subpage,
		       ]);

            }
            else
            {
	            return $this->render('index',[
	            'content' =>$content,
	            'city' => $city,
	            'subpage' => $subpage,
		       ]);
	    }

    }
}