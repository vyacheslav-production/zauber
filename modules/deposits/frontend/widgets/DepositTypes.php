<?php

namespace app\modules\deposits\frontend\widgets;

use app\modules\deposits\common\models\Dptypes;
use yii\base\Widget;
use yii\helpers\Url;

class DepositTypes extends Widget
{
    public $template = 'dptypes';

    public function run()
    {
	$exploded = explode("/",Url::to());
	if (!strlen($exploded[4]))
	     $uri ='main';
	else
	     $uri = $exploded[4];
	$city = $exploded[1];

        $types = Dptypes::find()->where(['dptypes_page'=>$uri])->andWhere(['dptypes_region'=>$city])->all();
        if (count($types)) {
            return $this->render($this->template, ['types' => $types]);
        } else {
            return '';
        }
    }
}