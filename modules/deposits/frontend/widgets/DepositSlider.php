<?php

namespace app\modules\deposits\frontend\widgets;

use app\modules\deposits\common\models\Dpslider;
use yii\base\Widget;
use yii\helpers\Url;

class DepositSlider extends Widget
{
    public $template = 'dpslider';

    public function run()
    {
	$exploded = explode("/",Url::to());
	if (!strlen($exploded[4]))
	     $uri ='main';
	else
	     $uri = $exploded[4];
	$city = $exploded[1];

        $slider = Dpslider::find()->where(['dpslider_page'=>$uri])->andWhere(['dpslider_region'=>$city])->all();
        if (count($slider)) {
            return $this->render($this->template, ['slider' => $slider]);
        } else {
            return '';
        }
    }
}