<?php

namespace app\modules\deposits\frontend\widgets;

use app\modules\deposits\common\models\Dpconditions;
use yii\base\Widget;
use yii\helpers\Url;

class DepositConditions extends Widget
{
    public $template = 'dpconditions';

    public function run()
    {
	$exploded = explode("/",Url::to());
	if (!strlen($exploded[4]))
	     $uri ='main';
	else
	     $uri = $exploded[4];
	$city = $exploded[1];

        $conditions = Dpconditions::find()->where(['dpconditions_page'=>$uri])->andWhere(['dpconditions_region'=>$city])->all();
        if (count($conditions)) {
            return $this->render($this->template, ['conditions' => $conditions]);
        } else {
            return '';
        }
    }
}