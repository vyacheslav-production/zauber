<?php

namespace app\modules\deposits\frontend\widgets;

use app\modules\deposits\common\models\Dphowto;
use yii\base\Widget;
use yii\helpers\Url;

class DepositHowto extends Widget
{
    public $template = 'dphowto';

    public function run()
    {
	$exploded = explode("/",Url::to());
	if (!strlen($exploded[4]))
	     $uri ='main';
	else
	     $uri = $exploded[4];
	$city = $exploded[1];

        $howto = Dphowto::find()->where(['dphowto_page'=>$uri])->andWhere(['dphowto_region'=>$city])->all();
        if (count($howto)) {
            return $this->render($this->template, ['howto' => $howto]);
        } else {
            return '';
        }
    }
}