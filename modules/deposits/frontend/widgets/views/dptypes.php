		<?php	foreach ($types as $item) { ?>
					<div class="container clear">
						<div class="heading" <?php if ($item['dptypes_img'] != '') {?>style="background:url(<?=$item['dptypes_img']?>)" <?php }?>>
							<div class="middle-holder">
								<div class="middle">
									<div class="inner">
										<h2><?=$item['dptypes_header']?></h2>
										<p><?=$item['dptypes_description']?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="content">
							<ul class="advantages-list type2 center">
			                        		   <?php echo $this->getDepositBlock($item['dptypes_block'])?> 
							</ul>
						</div>
					</div>
			<?php } ?>