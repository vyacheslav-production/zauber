			<?php foreach ($rsadvantages as $item) { ?>
				<div class="advantages-section">
					<div class="container clear">
						<div class="heading">
							<div class="middle-holder">
								<div class="middle">
									<h2><?= $item['rsadvantages_title']?></h2>
								</div>
							</div>
						</div>
						<div class="content">
							<ul class="advantages-list">
							   <?php echo $this->getBusinessCreditsAdvantagesBlock($item['rsadvantages_block'])?> 
							</ul>
						</div>
					</div>
				</div>
				<?php } ?>