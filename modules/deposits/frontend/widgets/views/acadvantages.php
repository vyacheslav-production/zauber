			<?php foreach ($acadvantages as $item) { 
			         if (strlen($item['acadvantages_img']) !== FALSE) 
			               $style= 'style =" background:url('.$item['acadvantages_img'].')!important"';
			         else
			               $style='';
				?>
				<div class="advantages-section">
					<div class="container clear">
						<div class="heading" <?= $style?> >
							<div class="middle-holder">
								<div class="middle">
									<h2><?= $item['acadvantages_header'] ?></h2>
								</div>
							   <?= $item['acadvantages_description'] ?>
							</div>
						</div>
						<div class="content">
							<ul class="advantages-list type2">
							   <?php echo $this->getAutoCreditAdvantagesBlock($item['acadvantages_block'])?> 
							</ul>

						</div>
					</div>
				</div>
			<?php } ?>
