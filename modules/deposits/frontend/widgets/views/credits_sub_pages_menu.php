<div class="top-menu-holder">
	<div class="container">
		<ul class="top-menu">
			<?php 
			foreach ($pages as $item) { ?>
				<li <?php echo ($uri == $item['bcpages_uri']) ? 'class="active"': '' ?> ><a href="/<?=$region?>/services/credits/<?=$item['bcpages_uri']?>/"><?=$item['bcpages_title']?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
