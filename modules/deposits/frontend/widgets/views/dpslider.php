<?php
  use app\modules\deposits\common\models\Dppages;
?>
	<ul class="slide-list">
			<?php 
			foreach ($slider as $item) { 
			            $uri = Dppages::find()->andWhere(['dppages_id'=>$item['dpslider_deposit_id']])->one()->dppages_uri;
?>
						<li>
							<span class="bg"><img src="<?= $item['dpslider_img']?>" alt="image description"></span>
							<div class="container">
								<div class="middle">
									<h2><?= $item['dpslider_header']?></h2>
									<p><?= $item['dpslider_description']?></p>
									<div class="btn-holder">
										<a data-anchor="anc-1" href="/spb/sevices/deposits/<?=$uri?>" class="btn btn-red scroll-btn">Подробнее о вкладе</a>
									</div>
								</div>
							</div>
						</li>
				<?php } ?>
		</ul>