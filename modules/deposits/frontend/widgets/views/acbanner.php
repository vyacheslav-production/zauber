			<?php 
			foreach ($acbanner as $item) { ?>

				<div class="intro" style="background-image:url(<?= $item['acbanners_img']?>);">
					<ul class="breadcrumbs">
						<li><a href="#">Для частных</a></li>
						<li class="active"><a href="#">Автокредитование</a></li>
					</ul>
					<div class="intro-content">
						<div class="middle">
							<div class="container">
								<div class="select-holder">
									<select>
										<option>Автокредит без посредников</option>
										<option>Кредит наличными</option>
									</select>
								</div>
								<h1><?= $item['acbanners_name']?></h1>
								<p><?= $item['acbanners_description']?></p>
								<a href="#steps" class="btn btn-red scroll-btn">Подать заявку на кредит</a>
							</div>
						</div>
					</div>
				<?php } ?>
