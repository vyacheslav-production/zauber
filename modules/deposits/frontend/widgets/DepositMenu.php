<?php

namespace app\modules\deposits\frontend\widgets;

use app\modules\deposits\common\models\Dppages;
use yii\base\Widget;
use yii\helpers\Url;

class DepositMenu extends Widget
{
    public $template = 'dpmenu';

    public function run()
    {
	$exploded = explode("/",Url::to());
	if (!strlen($exploded[3]))
	     $uri ='main';
	else
	     $uri = $exploded[3];
	$city = $exploded[1];
        $session = \Yii::$app->getSession();
	$type = ($session->has('client_type')) ? $session->get('client_type') : 'private';


        $menu = Dppages::find()->where(['dppages_type'=>$type])->andWhere(['dppages_region'=>$city])->all();
        if (count($menu)) {
            return $this->render($this->template, ['menu' => $menu]);
        } else {
            return '';
        }
    }
}