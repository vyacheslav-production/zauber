<?php

namespace app\modules\deposits\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "dpconditions".
 *
 * @property integer $dpconditions_id
 * @property string  $dpconditions_img
 * @property string  $dpconditions_title
 * @property string  $dpconditions_text
 * @property string  $dpconditions_page
 * @property string  $dpconditions_region

*/
class Dpconditions extends ActiveRecord
{

    static $image_width     = 1200;
    static $image_height    = 600;
    static $limit = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dpconditions';
    }
    public function rules()
    {
        return [
            [['dpconditions_img'], 'string', 'max' => 255],
            [['dpconditions_title'], 'string', 'max' => 255],
            [['dpconditions_text'], 'string', 'max' => 255],
            [['dpconditions_page'], 'string', 'max' => 255],
            [['dpconditions_region'], 'string', 'max' => 3],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dpconditions_id'           => 'ID',
            'dpconditions_img'          => 'Изображение',
            'dpconditions_title'        => 'Название',
            'dpconditions_text'         => 'Услуга',
            'dpconditions_page'         => 'Страница',
            'dpconditions_region'       => 'Регион',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->dpconditions_id;
    }

    /**
     * @inheritdoc
     * @return DpconditionsQuery
     */
    public static function find()
    {
        return new DpconditionsQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
