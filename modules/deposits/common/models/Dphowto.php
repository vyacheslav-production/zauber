<?php

namespace app\modules\deposits\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "dphowto".
 *
 * @property integer $dphowto_id
 * @property string  $dphowto_title
 * @property string  $dphowto_description
 * @property string  $dphowto_page
 * @property string  $dphowto_region
 */
class Dphowto extends ActiveRecord
{
    public static function tableName()
    {
        return 'dphowto';
    }
    public function rules()
    {
        return [
            [['dphowto_title1'], 'string', 'max' => 255],
            [['dphowto_description1'], 'string', 'max' => 255],
            [['dphowto_title2'], 'string', 'max' => 255],
            [['dphowto_description2'], 'string', 'max' => 255],
            [['dphowto_title3'], 'string', 'max' => 255],
            [['dphowto_description3'], 'string', 'max' => 255],
            [['dphowto_title4'], 'string', 'max' => 255],
            [['dphowto_description4'], 'string', 'max' => 255],
            [['dphowto_page'], 'string', 'max' => 255],
            [['dphowto_region'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dphowto_id'      => 'ID',
            'dphowto_title1'     => 'Заголовок 1',
            'dphowto_description1'  => 'Описание 1',
            'dphowto_title2'     => 'Заголовок 2',
            'dphowto_description2'  => 'Описание 2',
            'dphowto_title3'     => 'Заголовок 3',
            'dphowto_description3'  => 'Описание 3',
            'dphowto_title4'     => 'Заголовок 4',
            'dphowto_description4'  => 'Описание 4',

            'dphowto_page'  => 'Страница',
            'dphowto_region'  => 'Регион',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @inheritdoc
     * @return DphowtoQuery
     */
    public static function find()
    {
        return new DphowtoQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
