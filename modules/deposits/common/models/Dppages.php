<?php

namespace app\modules\deposits\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "dppages".
 *
 * @property integer $dppages_id
 * @property string  $dppages_title
 * @property string  $dppages_uri
 * @property string  $dppages_content
 * @property string  $dppages_region
*/
class Dppages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dppages';
    }
    public function rules()
    {
        return [
            [['dppages_title'], 'string', 'max' => 255],
            [['dppages_uri'], 'string', 'max' => 255],
            [['dppages_content'], 'string', 'max' => 65536],
            [['dppages_region'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dppages_id'          => 'ID',
            'dppages_title'       => 'Название',
            'dppages_uri' 	  => 'URI',
            'dppages_content' 	  => 'Контент',
            'dppages_region' 	  => 'Регион',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->dppages_id;
    }

    /**
     * @inheritdoc
     * @return DppagesQuery
     */
    public static function find()
    {
        return new DppagesQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
