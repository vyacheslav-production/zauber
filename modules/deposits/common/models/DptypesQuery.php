<?php

namespace app\modules\deposits\common\models;

use yii\db\ActiveQuery;

class DptypesQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}