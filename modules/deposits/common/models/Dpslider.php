<?php

namespace app\modules\deposits\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "dpslider".
 *
 * @property integer $dpslider_id
 * @property string  $dpslider_img
 * @property string  $dpslider_header
 * @property string  $dpslider_description
 * @property string  $dpslider_page
 * @property string  $dpslider_depost_id
 * @property string  $dpslider_region
 */
class Dpslider extends ActiveRecord
{
    static $image_width     = 1200;
    static $image_height    = 600;
    static $limit = 1;

    public static function tableName()
    {
        return 'dpslider';
    }
    public function rules()
    {
        return [
            [['dpslider_img'], 'string', 'max' => 255],
            [['dpslider_header'], 'string', 'max' => 255],
            [['dpslider_description'], 'string', 'max' => 255],
            [['dpslider_page'], 'string', 'max' => 255],
            [['dpslider_deposit_id'], 'string', 'max' => 1],
            [['dpslider_region'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dpslider_img'  => 'Изображение',
            'dpslider_header'  => 'Заголовок',
            'dpslider_description'  => 'Описание',
            'dpslider_page'  => 'Страница',
            'dpslider_deposit_id'  => 'ID вклада',
            'dpslider_region'  => 'Регион',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @inheritdoc
     * @return DpsliderQuery
     */
    public static function find()
    {
        return new DpsliderQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
