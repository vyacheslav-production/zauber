<?php

namespace app\modules\deposits\common\models;

use app\modules\core\common\models\Status;
use yii\db\ActiveQuery;

class DpconditionsQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}