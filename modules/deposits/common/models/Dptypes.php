<?php

namespace app\modules\deposits\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "dptypes".
 *
 * @property integer $dptypes_id
 * @property string  $dptypes_block
 * @property string  $dptypes_header
 * @property string  $dptypes_description
 * @property string  $dptypes_img
 */
class Dptypes extends ActiveRecord
{

    static $image_width     = 1200;
    static $image_height    = 600;
    static $limit = 1;

    public static function tableName()
    {
        return 'dptypes';
    }
    public function rules()
    {
        return [
            [['dptypes_block'], 'string', 'max' => 255],
            [['dptypes_header'], 'string', 'max' => 255],
            [['dptypes_description'], 'string', 'max' => 255],
            [['dptypes_img'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dptypes_id'              => 'ID',
            'dptypes_block'  	   => 'Блок с картинками',
            'dptypes_description'     => 'Описание',
            'dptypes_header'          => 'Заголовок',
            'dptypes_img'             => 'Изображение',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current silder item ID
     */
    public function getId()
    {
        return $this->dptypes_id;
    }

    /**
     * @inheritdoc
     * @return AcconditionsQuery
     */
    public static function find()
    {
        return new DptypesQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
