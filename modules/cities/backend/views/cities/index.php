<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\about\backend\models\About */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Страницы городов';
$this->subTitle                = 'Список';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить',
                           'url'     => ['create'],
                           'options' => ['class' => 'btn btn-success']
];
$this->params['menu'][] = ['label'   => '<i class="fa fa-fw fa-trash-o"></i> Корзина',
                           'url'     => ['recycle-bin'],
                           'options' => ['class' => 'btn btn-warning']
];
?>
<div class="program-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
		[                     
	            'label' => 'Id',
	            'attribute' =>'cities_id',
        	],
		[                     
	            'label' => 'URI',
	            'attribute' => 'cities_uri',
        	],
		[                     
	            'label' => 'Контент',
	            'attribute' => 'cities_content',
        	],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>

</div>
