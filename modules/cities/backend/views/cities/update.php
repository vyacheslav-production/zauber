<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\about\common\models\About */

$this->title = 'Страницы раздела городов';

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';

$this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-bars"></i> Список', 'url' => ['index'], 'options' => ['class' => 'btn btn-success']];

?>
<div class="program-update">
    <?= $this->render('_form', ['model' => $model,]) ?>

</div>
