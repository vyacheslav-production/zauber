<?php

namespace app\modules\cities\backend\controllers;

use app\modules\core\backend\components\ARController;
use Yii;
use app\modules\cities\common\models\Cities;
use app\modules\cities\backend\models\CitiesSearch;
use app\modules\core\backend\components\Controller;

/**
 * CitiesController implements the CRUD actions for City model.
 */
class CitiesController extends ARController
{

    protected function getModelClass()
    {   
        return Cities::className();
    }

    protected function getSearchModelClass()
    {
        return CitiesSearch::className();
    }


}
