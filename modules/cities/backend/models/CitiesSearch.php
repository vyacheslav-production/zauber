<?php

namespace app\modules\cities\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cities\common\models\Cities;

/**
 * CitiesSearch represents the model behind the search form about `app\modules\cities\common\models\Cities`.
 */
class CitiesSearch extends Cities
{
//    public $title, $category, $teacher;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cities::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $dataProvider;
    }
}
