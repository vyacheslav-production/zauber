<?php

namespace app\modules\cities\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "cities".
 *
 * @property integer $cities_id
 * @property string  $cities_uri
 * @property string  $cities_content
 */
class Cities extends ActiveRecord
{
    public static function tableName()
    {
        return 'cities';
    }
    public function rules()
    {
        return [
            [['cities_uri'], 'string', 'max' => 3],
            [['cities_content'], 'string', 'max' => 65536],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cities_id'      => 'ID',
            'cities_uri'     => 'URI',
            'about_content'  => 'Контент',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new CitiesQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
