<?php

namespace app\modules\cities\common\models;

use yii\db\ActiveQuery;

class CitiesQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}