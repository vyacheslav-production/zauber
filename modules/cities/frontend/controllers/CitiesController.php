<?php

namespace app\modules\cities\frontend\controllers;

use app\modules\cities\common\models\Cities;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;


class CitiesController extends \yii\web\Controller
{
  public $layout='cities.php';

    public function actionIndex()
    {

        $exploded = explode("/",Url::to());
        $cc = Cities::find()
            ->andWhere(['cities_uri' => $exploded[1]])
            ->one();
            return $this->render('index',[
            'content' => $cc->cities_content,
        ]);

    }
}