<?php

?>
<div class="sh-item">
    <a href="/spb/news/<?=date('Y',strtotime($model->news_date))?>/<?=date('m',strtotime($model->news_date))?>/<?=date('d',strtotime($model->news_date))?>/<?=$model->id?>/" class="sh-item-link">
        <div style="padding:20px 0 20px 0;">
            <b><?= $model->news_title; ?></b>
        </div>
        <div class="sh-item-desc">
            <?php
            $word        = htmlspecialchars(Yii::$app->request->queryParams['text']);
            $text        = strip_tags($model->news_content);
            $pos         = max(mb_stripos($text, $word, null, 'UTF-8') - 150, 0);
            $fragment    = mb_substr($text, $pos, 300, 'UTF-8');
            echo preg_replace("/($word)/i", "<b>$1</b>", $fragment );
            ?>
        </div>
    </a>
</div>