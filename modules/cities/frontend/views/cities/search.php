<?php
use app\modules\core\frontend\widgets\MainPageSlider;
use app\modules\core\frontend\widgets\MainPageBanners;
use app\modules\core\frontend\widgets\MainPageUnions;
use app\modules\core\frontend\widgets\MainPageFooter;
use app\modules\core\frontend\widgets\MainPageShownMenu;
use app\modules\core\frontend\widgets\MainPageHiddenMenu;
use app\modules\core\frontend\widgets\MainPageServices;
use app\modules\core\frontend\widgets\MainPageNews;
use app\modules\core\frontend\widgets\MainPageCitySelect;
?>
		<div id="wrapper">
			<header id="header">
				<div class="container">
					<div class="contact-panel">
						<strong class="logo"><a href="#">Заубер Банк</a></strong>
			                        <?= MainPageCitySelect::widget(); ?>
						<div class="contacts">
							<a href="tel:+78126470606" class="tel">+7 (812) <strong>647-06-06</strong></a>
							<div class="link-holder">
								<a href="#" class="link">Контактная информация</a>
							</div>
						</div>
						<div class="select-holder pull-right style2 unchanged">
							<select>
								<option>Для физических лиц</option>
								<option>Для юридических лиц</option>
							</select>
						</div>
						<a href="#" class="burger"><span>&nbsp;</span></a>
					</div>
				</div>
				<nav id="nav">
					<div class="container">
						<div class="nav-inner">
							<div class="change-list-holder">
								<ul class="change-list">
									<li class="changetype pli" clientType="private"><a href="#">Частным лицам</a></li>
									<li class="changetype bli" clientType="business"><a href="#">Для бизнеса</a></li>
								</ul>
							</div>
							<div class="menu-holder">
								<a href="#" class="close">X</a>
						                        <?= MainPageShownMenu::widget(); ?>
							</div>
							<div class="drop-holder">
								<a href="#" class="title">Другие услуги</a>
							</div>
							<form action="<?= \yii\helpers\Url::toRoute('/search/')?>" class="search-form">
								<fieldset>
				                                        <input type="search" name="text" class="text" value="<?= htmlspecialchars(Yii::$app->request->queryParams['text'])?>" placeholder="Поиск">
									<input type="submit" value="search">
								</fieldset>
							</form>
						</div>
					</div>
					<div class="drop-manu">
						<div class="container hiddenitems">
				                        <?= MainPageHiddenMenu::widget(); ?>
						</div>
					</div>
				</nav>
			</header>
			<div id="main" class="search-results">
			    <h2 class="g-hd sh-hd">
			        Результаты поиска
			    </h2>

			    <div>
			        <div class="sh-lead">
			            <div class="search-count">
			                <?php if (!empty(Yii::$app->request->queryParams['text'])) : ?>
			                    <?= $count; ?> результатов
			                <?php endif ?>
			            </div>
			        </div>
			        <?php if (!empty(Yii::$app->request->queryParams['text'])) : ?>
			            <div class="sh-grp">
			                        <?= \yii\widgets\ListView::widget([
			                            'dataProvider' => $dataProvider,
			                            'itemView'     => '_search_single.php',
			                            'layout'       => '{items}',
			                            'itemOptions'  => ['tag' => false]
			                        ]);
			                        ?>
		            </div>
		            <div class="loading-s">
		            <?php if (!empty(Yii::$app->request->queryParams['text']) && $count > 10 ) : ?>
		                <a href="<?= \yii\helpers\Url::toRoute(['/search/', 'text' => htmlspecialchars(Yii::$app->request->queryParams['text'])]); ?>" data-page="2" data-pages="<?= $pages; ?>" class="form-btn transparent loading-btn loading-sh-btn">
		                    Показать ещё
			                </a>
			                <img src="/images/loading.gif" class="loading-pic" alt="">
					    <?php endif ?>
			            </div>
			        </div>
			    <?php else : ?>
			        Задан пустой поисковый запрос
			    <?php endif ?>
			</div>
                        <?= MainPageFooter::widget(); ?>
		</div>