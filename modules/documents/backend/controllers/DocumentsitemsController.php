<?php

namespace app\modules\documents\backend\controllers;
use Yii;
use app\modules\documents\backend\models\DocumentsSearch;
use app\modules\documents\common\models\Documents;
use yii\helpers\ArrayHelper;

use app\modules\core\backend\components\ARController;

class DocumentsitemsController extends ARController
{

    public function beforeAction($action)
    {
        $this->model_class = Documents::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['create'], $actions['update']);

        return $actions;
    }

    protected function getModelClass()
    {
        return Documents::className();
    }

    protected function getSearchModelClass()
    {          
        return DocumentsSearch::className();
    }
    public function actionUpdate($id)
    {
        /** @var Documents $model */
        $model = Documents::findOne($id);

        if (Yii::$app->request->isPost) {


        $ret = [];
	if (ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_name') !== NULL)
	{
	        $nam =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_name'));
	        $doc =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_file'));
	        $grp =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_group'));
	        $ord =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_order'));
	        $ret =[];
	        foreach ($nam as $key=>$value)
	        {	
	              $arr = [];
	              $arr['name'] = $value;
	              $arr['doc'] = $doc[$key];
	              $arr['group'] = $grp[$key];
	              $arr['order'] = $ord[$key];
	              array_push($ret,$arr);
	        }
	}

	$model->load(Yii::$app->request->post());
	$model->documents_block = serialize($ret);
        if ($model->save()) {
            return $this->redirect(['index']);
        } 

      }  
        return $this->render('update', [
            'model' => $model,
        ]);
    }


}
