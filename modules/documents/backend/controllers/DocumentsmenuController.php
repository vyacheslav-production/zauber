<?php

namespace app\modules\documents\backend\controllers;
use Yii;
use app\modules\documents\common\models\Documentsmenu;
use app\modules\documents\backend\models\DocumentsmenuSearch;
use app\modules\attributes\common\models\Attributes;

use app\modules\core\backend\components\ARController;

class DocumentsmenuController extends ARController
{

    public function beforeAction($action)
    {
        $this->model_class = Documents::class;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        unset ($actions['create'], $actions['update']);

        return $actions;
    }

    protected function getModelClass()
    {
        return Documentsmenu::className();
    }

    protected function getSearchModelClass()
    {
        return DocumentsmenuSearch::className();
    }


    public function actionUpdate($id)
    {
        /** @var Documentsmenu $model */
        $model = Documentsmenu::findOne($id);

        if (Yii::$app->request->isPost) {


        $ret = [];
	if (ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_name') !== NULL)
	{
	        $nam =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_name'));
	        $doc =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_file'));
	        $grp =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_group'));
	        $ord =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_order'));
	        $ret =[];
	        foreach ($nam as $key=>$value)
	        {	
	              $arr = [];
	              $arr['name'] = $value;
	              $arr['doc'] = $doc[$key];
	              $arr['group'] = $grp[$key];
	              $arr['order'] = $ord[$key];
	              array_push($ret,$arr);
	        }
	}

	$model->load(Yii::$app->request->post());
	$model->documents_menu_block = serialize($ret);
        if ($model->save()) {
            return $this->redirect(['index']);
        } 

      }  
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionCreate()
    {
        /** @var Documentsmenu $model */
        $model = new Documentsmenu;

        if (Yii::$app->request->isPost) {


        $ret = [];
	if (ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_name') !== NULL)
	{
	        $nam =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_name'));
	        $doc =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_file'));
	        $grp =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_group'));
	        $ord =explode('&',ArrayHelper::getValue(Yii::$app->request->post(), 'docblock_order'));
	        $ret =[];
	        foreach ($nam as $key=>$value)
	        {	
	              $arr = [];
	              $arr['name'] = $value;
	              $arr['doc'] = $doc[$key];
	              $arr['group'] = $grp[$key];
	              $arr['order'] = $ord[$key];
	              array_push($ret,$arr);
	        }
	}

	$model->load(Yii::$app->request->post());
	$model->documents_menu_block = serialize($ret);
        if ($model->save()) {
            return $this->redirect(['index']);
        } 

      }  
        return $this->render('update', [
            'model' => $model,
        ]);
    }

}
