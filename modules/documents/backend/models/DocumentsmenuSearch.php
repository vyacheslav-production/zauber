<?php

namespace app\modules\documents\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\documents\common\models\Documentsmenu;

/**
 * DocumentsmenuSearch represents the model behind the search form about `app\modules\documentsmenu\common\models\Documentsmenu`.
 */
class DocumentsmenuSearch extends Documentsmenu
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Documentsmenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $dataProvider;
    }
}
