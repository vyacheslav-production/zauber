<?php

return [
    'documentsitems/<action:[\w\-]+>' => '/documents/documentsitems/<action>',
    'documentsmenu/<action:[\w\-]+>' => '/documents/documentsmenu/<action>',
];