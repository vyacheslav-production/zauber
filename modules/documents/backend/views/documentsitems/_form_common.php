<?php

use yii\helpers\Html;
Use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\documents\common\models\Documents */
/* @var $form yii\widgets\ActiveForm */
/* */
?>
<?php echo Html::Input('hidden','docblock_name','',['id'=>'docblock_nam']) ?> 
<?php echo Html::Input('hidden','docblock_file','',['id'=>'docblock_doc']) ?> 
<?php echo Html::Input('hidden','docblock_group','',['id'=>'docblock_grp']) ?> 
<?php echo Html::Input('hidden','docblock_order','',['id'=>'docblock_ord']) ?> 
<?= $form->field($model, 'documents_header')->textInput(['maxlength' => 30]) ?>
