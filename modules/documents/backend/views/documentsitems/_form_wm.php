<?php

use yii\helpers\Html;
Use yii\helpers\Url;

?>
<div class="unfold">
<?php foreach (unserialize($model->documents_block) as $key=>$value) {?>
	<label class="control-label <?php echo 'documents'.$key?>">Документ</label>
		<?php echo Html::Input('hidden','documents[]', $value['doc'],['class'=>'documents'.$key, 'id'=>'doc_name'.$key]) ?>
   				<?php echo Html::beginForm('','post',['id'=>'wmform','class'=>'form_upload_wm','action'=>'','id'=>$key,'enctype'=>'multipart/form-data']) ?>
				<?php echo Html::fileInput('fi','',['class'=>'doc_upload_wm','id'=>'fi','data-url'=>Url::to(['ajax-upload-wm'])]) ?>
   				<?php echo Html::endForm() ?>
	<br>
	<label class="control-label <?php echo 'name'.$key?>">Название</label>
		<?php echo Html::input('name','name[]',$value['name'],['class'=>'name'.$key]) ?>
	<br><br>
	<label class="control-label <?php echo 'group'.$key?>">Группировка</label>
		<?php echo Html::input('group','group[]',$value['group'],['class'=>'group'.$key]) ?>
	<br><br>
	<label class="control-label <?php echo 'order'.$key?>">Порядок</label>
		<?php echo Html::input('order','order[]',$value['order'],['class'=>'order'.$key]) ?>
	<br><br>
        <?php echo Html::Button('Удалить', ['class' => 'btn btn-primary deldoc','id' => $key ]);?>
	<br><br>
<?php }?>
</div>
	<br><br>
        <?php echo Html::Button('Добавить', ['class' => 'btn btn-primary adddoc' ]);?>
	<br><br>

