<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\aсrequired\backend\models\Aсrequired */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Документы';
$this->subTitle                = 'Список';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="program-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
		[                     
	            'label' => 'Заголовок',
	            'attribute' =>'documents_header',
        	],

		[                     
	            'label' => 'Блок с документами',
	            'attribute' => 'documents_block',
	            'value' => function($data) {
	               		return ( $this->getDocumentsBlockProperties($data->documents_block));
		            },

        	],
           ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>

</div>
