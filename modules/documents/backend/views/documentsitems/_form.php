<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\services\common\models\Services */
/* @var $form yii\widgets\ActiveForm */
/* @var $upload_form app\modules\core\backend\components\UploadForm */

echo Html::beginTag('div');
echo $this->render('_form_wm', ['model' => $model]);
$form = ActiveForm::begin(['id'=>'documents']);
echo Html::beginTag('div', ['class' => 'form-group']);
echo $this->render('_form_common', ['model' => $model, 'form' => $form]);
//echo Html::Button('Сохранить', ['class' => 'btn btn-primary advfold']);
echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary' ]);
ActiveForm::end();
echo Html::endTag('div');
