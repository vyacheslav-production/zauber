<?php

use yii\grid\GridView;

/* @var $this app\modules\core\backend\components\View */
/* @var $searchModel app\modules\documents\backend\common\Documentsmenu */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пункты меню';
$this->subTitle = 'Список';
$this->params['breadcrumbs'][] = $this->title;

 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-plus-square-o"></i>Добавить', 'url' => ['create'], 'options' => ['class' => 'btn btn-success']];
 $this->params['menu'][] = ['label' => '<i class="fa fa-fw fa-trash-o"></i> Корзина', 'url' => ['recycle-bin'], 'options' => ['class' => 'btn btn-warning']];

?>
<div class="user-index">
    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'columns' => [
		[                     
	            'label' => 'Id',
	            'attribute' => 'documents_menu_id',
        	],
		[                     
	            'label' => 'Название',
	            'attribute' => 'documents_menu_title',
        	],
		[                     
	            'label' => 'URI',
	            'attribute' => 'documents_menu_uri',
        	],
		[                     
	            'label' => 'Блок с документами',
	            'attribute' => 'documents_menu_block',
        	],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>

</div>
