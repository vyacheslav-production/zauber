<?php

namespace app\modules\documents\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "documents".
 *
 * @property integer $documents_menu_id
 * @property string  $documents_menu_title
 */
class Documentsmenu extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents_menu';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ],
            ],
            'softDelete' => [
                'class' => SoftDeleteBehavior::className()
            ],
        ];
    }
    public function rules()
    {
        return [
            [['documents_menu_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'documents_menu_id'        => 'ID',
            'documents_menu_title'     => 'Название пункта меню',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current menu item ID
     */
    public function getId()
    {
        return $this->documents_menu_id;
    }

    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new DocumentsmenuQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
