<?php

namespace app\modules\documents\common\models;

use app\modules\core\common\behaviors\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use app\modules\core\common\components\ActiveRecord;

/**
 * This is the model class for table "documents".
 *
 * @property integer $documents_id
 * @property string  $documents_header
 * @property string  $documents_block
 */
class Documents extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ],
            ],
            'softDelete' => [
                'class' => SoftDeleteBehavior::className()
            ],
        ];
    }
    public function rules()
    {
        return [
            [['documents_header'], 'string', 'max' => 255],
            [['documents_block'], 'string', 'max' => 65536],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'documents_id'         => 'ID',
            'documents_header'     => 'Заголовок документов',
            'documents_block'      => 'Блок документов',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */

    public static function findIdentity($id)
    {
        $model = static::findOne($id);
        return $model;
    }
    /**
     * @return int|string current menu item ID
     */
    public function getId()
    {
        return $this->documents_id;
    }

    /**
     * @inheritdoc
     * @return UserQuery
     */
    public static function find()
    {
        return new DocumentsQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        return true;
    }

}
