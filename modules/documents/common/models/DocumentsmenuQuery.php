<?php

namespace app\modules\documents\common\models;

use app\modules\core\common\models\Status;
use yii\db\ActiveQuery;

class DocumentsmenuQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function active($state = true)
    {
        return $this;
    }

}